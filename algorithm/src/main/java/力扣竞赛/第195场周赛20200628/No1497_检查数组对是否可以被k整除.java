package 力扣竞赛.第195场周赛20200628;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/25 17:12
 */
public class No1497_检查数组对是否可以被k整除 {
    public static void main(String[] args) {
        Solution1497 solution1497 = new Solution1497();
        int[] arr = new int[]{-1, -1, -1, -1, 2, 2, -2, -2};
        boolean b = solution1497.canArrange(arr, 3);
        System.out.println(b);
    }
}

class Solution1497 {
    public boolean canArrange(int[] arr, int k) {
        //余数法
        //mod[n]=A:代表余数为n的有A个
        int[] mod = new int[k];
        for (int data : arr) {
            int yu = data % k;
            if (yu < 0) {
                yu = yu + k;
            }
            mod[yu] += 1;
        }
        System.out.println();
        for (int i = 1; i <= k / 2; i++) {
            if (mod[i] != mod[k - i]) {
                return false;
            }
        }
        return mod[0] % 2 == 0;

    }
}
