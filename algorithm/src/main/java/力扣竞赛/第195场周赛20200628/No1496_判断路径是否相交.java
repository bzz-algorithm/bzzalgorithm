package 力扣竞赛.第195场周赛20200628;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/25 12:48
 */
public class No1496_判断路径是否相交 {
    public static void main(String[] args) {
        Solution1496 solution1496 = new Solution1496();
        boolean neenws = solution1496.isPathCrossing("NEENWS");
        System.out.println(neenws);
    }
}

class Solution1496 {
    public boolean isPathCrossing(String path) {
        int x = 0;
        int y = 0;
        StringBuffer sb = new StringBuffer();
        List<String> list = new ArrayList<>();
        list.add("0,0");
        for (int i = 0; i < path.length(); i++) {
            char check = path.charAt(i);
            if ('N' == check) {
                y++;
            } else if ('S' == check) {
                y--;
            } else if ('E' == check) {
                x++;
            } else if ('W' == check) {
                x--;
            }
            if (list.contains((x + "," + y))) {
                return true;
            }
            list.add(x + "," + y);
        }
        return false;
    }
}