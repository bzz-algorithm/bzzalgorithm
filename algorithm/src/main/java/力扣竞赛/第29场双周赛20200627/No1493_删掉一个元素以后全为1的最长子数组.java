package 力扣竞赛.第29场双周赛20200627;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/5 18:39
 */
public class No1493_删掉一个元素以后全为1的最长子数组 {
    public static void main(String[] args) {
        Solution1493 solution1493 = new Solution1493();
        int[] nums = new int[]{1,1, 1};
        int longestSubarray = solution1493.longestSubarray(nums);
        System.out.println(longestSubarray);
    }
}

class Solution1493 {
    public int longestSubarray(int[] nums) {
        int right = 0;
        int left = 0;
        int max = 0;
        boolean flag = true; // 全为1为true
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                right++;
            } else {
                left = right;
                right = 0;
                flag = false;
            }
            max = Math.max(left + right, max);
        }
        if (flag) {
            return max - 1;
        } else {
            return max;
        }
    }
}
