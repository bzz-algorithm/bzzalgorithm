package 力扣竞赛.第29场双周赛20200627;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/5 18:36
 */
public class No1492_n的第k个因子 {
    public static void main(String[] args) {
        Solution1492 solution1492 = new Solution1492();
        //12的第三个因子
        int i = solution1492.kthFactor(12, 3);
        System.out.println(i);
    }
}

class Solution1492 {
    public int kthFactor(int n, int k) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                list.add(i);
            }
        }
        try {
            return list.get(k - 1);
        } catch (Exception e) {
            return -1;
        }
    }
}