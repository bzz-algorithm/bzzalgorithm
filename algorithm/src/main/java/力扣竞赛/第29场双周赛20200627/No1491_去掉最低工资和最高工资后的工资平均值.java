package 力扣竞赛.第29场双周赛20200627;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/5 18:31
 */
public class No1491_去掉最低工资和最高工资后的工资平均值 {
    public static void main(String[] args) {
        Solution1491 solution1491 = new Solution1491();
        int[] salary = new int[]{4000, 3000, 1000, 20000};
        double average = solution1491.average(salary);
        System.out.println(average);
    }
}

class Solution1491 {
    public double average(int[] salary) {
        if (salary.length == 0) {
            return 0.0;
        }
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double sum = 0.0;
        for (int i = 0; i < salary.length; i++) {
            sum += Double.valueOf(salary[i]);
            min = Math.min(salary[i], min);
            max = Math.max(salary[i], max);
        }
        return (sum - min - max) / (salary.length - 2);
    }
}
