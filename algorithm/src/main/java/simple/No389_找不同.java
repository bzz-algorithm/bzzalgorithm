package simple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/11/26 23:00
 */
public class No389_找不同 {
    public static void main(String[] args) {
        Solution389 solution389 = new Solution389();
        String s = "abcd";
        String t = "abcde";
        char theDifference = solution389.findTheDifference(s, t);
        System.out.println(theDifference);
    }
}

class Solution389 {
    public char findTheDifference(String s, String t) {
        //异或
        //abcd
        //cbadu
        //0^A=A
        //A^A=0
        int res = 0;
        //对s进行全量异或
        for (char ss : s.toCharArray()) {
            res = res ^ ss;
        }
        for (char tt : t.toCharArray()) {
            res = res ^ tt;
        }
        return (char) res;
    }
}



    //public char findTheDifference(String s, String t) {
    //    //时间复杂度O(n),空间复杂度O(1)???
    //    //有
    //    //5789     ->   29
    //    //96875    ->   35    6
    //    //abcd -> 'a' -> int
    //
    //    //比较差值法
    //    int sSum = 0;//s字符串总和
    //    int tSum = 0;//t字符串总和
    //    for (int i = 0; i < s.length(); i++) {
    //        sSum += s.charAt(i);
    //        tSum += t.charAt(i);
    //    }
    //    //t最后一位计入
    //    tSum += t.charAt(t.length() - 1);
    //
    //    return (char) (tSum - sSum);
    //}



    //public char findTheDifference(String s, String t) {
    //    //哈希
    //    //形成map(s,t)
    //    Map<Character, Integer> sMap = new HashMap<>();
    //    Map<Character, Integer> tMap = new HashMap<>();
    //
    //    char[] charsS = s.toCharArray();
    //    char[] charsT = t.toCharArray();
    //
    //    for (int i = 0; i < charsS.length; i++) {
    //        addMapKeys(tMap, charsT[i]);
    //        addMapKeys(sMap, charsS[i]);
    //    }
    //    //t最后一位计入
    //    addMapKeys(tMap, charsT[charsT.length - 1]);
    //
    //    //1.map形成之后,取tMap进行遍历
    //    for (Map.Entry<Character, Integer> data : tMap.entrySet()) {
    //        char key = data.getKey();
    //        //从t里面一个一个字符在s里面找
    //        //2.tMap.get(key) != sMap.get(t) -> t
    //        if (tMap.get(key) != sMap.get(key)) {
    //            return key;
    //        }
    //    }
    //
    //    return '好';
    //
    //}
    //
    ////熟悉的方法
    //public void addMapKeys(Map<Character, Integer> map, char key) {
    //    if (map.get(key) == null) {
    //        map.put(key, 1);
    //    } else {
    //        map.put(key, map.get(key) + 1);
    //    }
    //}



    //public char findTheDifference(String s, String t) {
    //    //abcd
    //    //cabud  -> u
    //
    //    //排回去,不要乱排!
    //    //排序法
    //    //获取数组
    //    char[] charsS = s.toCharArray();
    //    char[] charsT = t.toCharArray();
    //    //排序结束
    //    Arrays.sort(charsS);
    //    Arrays.sort(charsT);
    //
    //    //遍历短字符串
    //    for (int i = 0; i < charsS.length; i++) {
    //        //1.遇到不相同,取长字符串位置
    //        if (charsS[i] != charsT[i]) {
    //            return charsT[i];
    //        }
    //    }
    //
    //    //2 遍历结束没有遇到不同,取长字符串最后位置
    //    return charsT[charsT.length - 1];
    //}
