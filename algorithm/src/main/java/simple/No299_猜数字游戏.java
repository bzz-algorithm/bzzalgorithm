package simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/9/13 11:55
 */
public class No299_猜数字游戏 {
    public static void main(String[] args) {

    }
}

class Solution299 {
    public String getHint(String secret, String guess) {
        //双映射
        char[] c1 = secret.toCharArray();
        char[] c2 = guess.toCharArray();
        int g = 0;//公牛数量
        int n = 0;//奶牛数量
        //公牛初始化
        Map<Character, Integer> map1 = new HashMap<>();
        Map<Character, Integer> map2 = new HashMap<>();
        for (int i = 0; i < c2.length; i++) {
            addMapKeys(map1, c1[i]);
        }

        //计算奶牛
        for (int i = 0; i < c2.length; i++) {
            //顺便计算公牛
            if (c1[i] == c2[i]) {
                g++;
            }
            //正式计算奶牛
            if (map1.get(c2[i]) == null) {
                continue;
            } else {
                if (map2.get(c2[i]) == null || map2.get(c2[i]) < map1.get(c2[i])) {
                    n++;
                    addMapKeys(map2, c2[i]);
                }
            }

        }
        return g + "A" + (n - g) + "B";
    }

    public void addMapKeys(Map<Character, Integer> map, char key) {
        if (map.get(key) == null) {
            map.put(key, 1);
        } else {
            map.put(key, map.get(key) + 1);
        }
    }
}


    //public String getHint(String secret, String guess) {
    //    //xxx1x
    //    //xxx1x 1个公牛
    //
    //    //2xx1x
    //    //xx212 1个公牛,1个奶牛
    //    //暴力遍历
    //    char[] c1 = secret.toCharArray();
    //    char[] c2 = guess.toCharArray();
    //    int g = 0;//公牛
    //    int n = 0;//奶牛
    //    //先找公牛
    //    for (int i = 0; i < c2.length; i++) {
    //        if (c1[i] == c2[i]) {
    //            g++;
    //            c1[i] = 'A';
    //            c2[i] = 'A';
    //        }
    //    }
    //
    //
    //    for (int i = 0; i < c1.length; i++) {
    //        for (int j = 0; j < c2.length; j++) {
    //            char check = c1[i];
    //            if (c1[i] == 'A' || c1[i] == 'B') {
    //                continue;
    //            } else {
    //                if (c1[i] == c2[j]) {
    //                    n++;
    //                    c1[i] = 'B';
    //                    c2[j] = 'B';
    //                }
    //            }
    //        }
    //    }
    //    return g + "A" + n + "B";
    //
    //
    //}






