package simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/13 23:08
 */
public class No118_杨辉三角 {
    public static void main(String[] args) {
        Solution118 solution118 = new Solution118();
        List<List<Integer>> res = solution118.generate(8);
        System.out.println();
    }
}

class Solution118 {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> res = new ArrayList<>();
        if (numRows == 0) {
            return res;
        }
        //头
        List<Integer> begin = new ArrayList<>();
        begin.add(1);
        res.add(begin);
        for (int i = 2; i <= numRows; i++) {
            //迭代begin
            begin = getData(begin);
            res.add(begin);
        }
        return res;
    }

    //头 [1]
    public List<Integer> getData(List<Integer> data) {
        int length = data.size();
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i <= length - 2; i++) {
            //计算出来的数字
            int n = data.get(i) + data.get(i + 1);
            res.add(n);
        }
        //头尾补1
        res.add(0, 1);
        res.add(1);
        return res;
    }
}