package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/4/6 22:27
 */
public class No492_构造矩形 {
    public static void main(String[] args) {
        Solution492 solution492 = new Solution492();
        int[] ints = solution492.constructRectangle(7788);
        System.out.println(ints);
    }
}

class Solution492 {
    public int[] constructRectangle(int area) {
        // 双指针
        int l = area;
        int w = 1;
        int[] res = new int[2];
        while (l >= w) {
            if (l * w == area) {
                //获取res
                res[0] = l;
                res[1] = w;
                //继续运作
                l--;
            } else if (l * w < area) {
                //面积要扩大,趋向于相等
                w++;
            } else if (l * w > area) {
                //面积减小
                l--;
            }
        }
        return res;
    }
}



    //public int[] constructRectangle(int area) {
    //    //数学计算,暴力取数
    //    int[] res = new int[]{area, 1};
    //    //获取接近与area的w
    //    //88
    //    int base = (int) Math.sqrt(area);
    //    //暴力遍历到1
    //    for (int w = base; w >= 1; w--) {
    //        if (area % w == 0) {
    //            //可以整除,直接返回
    //            res[0] = area / w;
    //            res[1] = w;
    //            break;
    //        }
    //    }
    //    return res;
    //}













