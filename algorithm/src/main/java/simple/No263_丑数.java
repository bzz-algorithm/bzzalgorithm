package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/8/9 16:51
 */
public class No263_丑数 {
    public static void main(String[] args) {
        Solution263 solution263 = new Solution263();
        boolean ugly = solution263.isUgly(43);
        System.out.println(ugly);
    }
}

class Solution263 {
    public boolean isUgly(int num) {
        if (num == 0) {
            return false;
        }
        while (num % 2 == 0) {
            num = num / 2;
        }
        //无法被2整除之后的操作 //15
        while (num % 3 == 0) {
            num = num / 3;
        }
        //无法被3整除之后的操作 //5
        while (num % 5 == 0) {
            num = num / 5;
        }
        //无法被5整除之后的操作
        return num == 1;
    }
}
