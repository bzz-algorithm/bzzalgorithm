package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/2/5 13:59
 */
public class No415_字符串相加 {
    public static void main(String[] args) {
        Solution415 solution415 = new Solution415();
        String s = solution415.addStrings("99976", "58");
        System.out.println(s);

    }
}

class Solution415 {
    public String addStrings(String num1, String num2) {
        // "99976" + "58" = "100034"
        //指针定义
        int red = num1.length() - 1;
        int green = num2.length() - 1;
        //进位初始化
        int jin = 0;

        StringBuffer res = new StringBuffer();

        //开始循环
        while (red >= 0 || green >= 0 || jin == 1) {
            //红绿指针指向的数(索引小于0进行处理)
            int redNum = red >= 0 ? num1.charAt(red) - '0' : 0;
            int greenNum = green >= 0 ? num2.charAt(green) - '0' : 0;

            //进位加上总和
            int plus = redNum + greenNum + jin;
            //一个数
            int ge = plus % 10;

            //改变jin
            jin = plus / 10;

            res.append(ge);
            red--;
            green--;
        }

        return res.reverse().toString();

    }
}






