package simple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/8 14:56
 */
public class No1_两数之和 {
    public static void main(String[] args) {
        //int[] nums = {1, 13, 23, 17, 19, 22, 37}; // 2,3
        int[] nums = {3,2,4}; // 2,3
        int target = 6;
        Solution1 solution1 = new Solution1();

        int[] ints = solution1.twoSum(nums, target);
        System.out.println();
    }
}


class Solution1 {
    public int[] twoSum(int[] nums, int target) {
        //3.一次性哈希
        Map<Integer, Integer> map = new HashMap<>();
        //一次遍历nums数组
        for (int i = 0; i < nums.length; i++) {
            int check = target - nums[i]; // 39
            if (map.containsKey(check)) {
                return new int[]{i, map.get(check)};
            }
            map.put(nums[i], i);
        }
        return null;
    }
}



//public int[] twoSum(int[] nums, int target) {
//    //双循环哈希映射
//    Map<Integer, Integer> map = new HashMap<>();
//    int length = nums.length;
//    //哈希数据填充:(数据,索引)
//    for (int i = 0; i < length; i++) {
//        map.put(nums[i], i);
//    }
//
//    //找目标
//    for (int i = 0; i < length; i++) {
//        int check = target - nums[i]; // 39
//        if (map.containsKey(check) && i != map.get(check)) {
//            int j = map.get(check);
//            return new int[]{i, j};
//        }
//    }
//    return null;
//}

//public int[] twoSum(int[] nums, int target) {
//    //1.暴力法
//    int length = nums.length;
//    for (int i = 0; i < length; i++) {
//        int check = nums[i];
//        //另外一个数组
//        for (int j = 0; j < length; j++) {
//            if (i != j && nums[j] == target - check) {
//                return new int[]{i, j};
//            }
//        }
//    }
//    return null;
//}