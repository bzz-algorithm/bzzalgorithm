package simple;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/8/9 23:04
 */
public class No283_移动零 {
    public static void main(String[] args) {
        Solution283 solution283 = new Solution283();
        int[] nums = new int[]{0, 1, 2, 0, 7, 8, 0, 3};
        solution283.moveZeroes(nums);
        System.out.println();
    }
}

class Solution283 {
    public void moveZeroes(int[] nums) {
        //双指针
        int red = 0;
        for (int green = 0; green < nums.length; green++) {
            if (nums[green] != 0) {
                //将red位置数替换为nums[green]
                nums[red++] = nums[green];
            }
        }
        for (int i = red; i < nums.length; i++) {
            nums[i] = 0;
        }
    }
}
