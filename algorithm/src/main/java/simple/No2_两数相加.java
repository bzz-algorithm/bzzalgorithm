package simple;

import data.ListNode;

import java.math.BigDecimal;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/8 21:44
 */
public class No2_两数相加 {
    public static void main(String[] args) {
        Solution2 solution2 = new Solution2();
        //模拟 684352 + 7659 = 692011
        ListNode l1 = new ListNode(2);
        l1.add(5);
        l1.add(3);
        l1.add(4);
        l1.add(8);
        l1.add(6);

        ListNode l2 = new ListNode(9);
        l2.add(5);
        l2.add(6);
        l2.add(7);

        ListNode listNode = solution2.addTwoNumbers(l1, l2);
        listNode.show(); // 1->1->0->2->9->6

        System.out.println();
    }
}

class Solution2 {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //2.进位添数法
        //头法
        ListNode result = new ListNode(-1);
        int jin = 0;
        int gSum = 0;
        while (l1 != null || l2 != null) {
            if (l1 != null && l2 != null) {
                gSum = l1.val + l2.val + jin;
                jin = gSum > 9 ? 1 : 0;
                //链表元素添加
                addList(result, gSum % 10);
            } else if (l1 == null) {
                if (l2 == null) {
                    break;
                } else {
                    gSum = l2.val + 0 + jin;
                    jin = gSum > 9 ? 1 : 0;
                    addList(result, gSum % 10);
                }
            } else if (l2 == null) {
                if (l1 == null) {
                    break;
                } else {
                    gSum = l1.val + 0 + jin;
                    jin = gSum > 9 ? 1 : 0;
                    addList(result, gSum % 10);
                }
            }
            if(l1 != null) l1 = l1.next;
            if(l2 != null) l2 = l2.next;
        }
        //最高位大于9,进行处理
        if (jin != 0) {
            addList(result, 1);
        }

        return result.next;
    }

    //链表元素添加
    public void addList(ListNode head, int data) {
        ListNode headCp = head;
        while (head != null && head.next != null) {
            head = head.next;
        }
        head.next = new ListNode(data);
        head = headCp;
    }


}



//public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
//    //1.暴力法,强行硬写,投机取巧,java专用,其他语言不推荐
//    BigDecimal num1 = getNums(l1);
//    BigDecimal num2 = getNums(l2);
//    BigDecimal result = num1.add(num2); //12011
//    ListNode listNode = num2List(result);
//    return listNode;
//}
//
////链表转整数
//public BigDecimal getNums(ListNode l1) {
//    StringBuffer sb = new StringBuffer();
//    //把l1保存下来
//    ListNode liCp = l1;
//    int val = l1.val; // 2
//    sb.append(val);
//    while (l1 != null && l1.next != null) {
//        l1 = l1.next; // 5
//        val = l1.val; // 5
//        sb.append(val);
//    }
//    //刷回去
//    l1 = liCp;
//    String s = sb.reverse().toString(); // 4352
//    return new BigDecimal(s);
//}
//
////整数转回链表
//public ListNode num2List(BigDecimal num) {
//    String s = num + ""; // "12011"
//    StringBuffer sb = new StringBuffer();
//    sb.append(s);
//    String reverse = sb.reverse().toString(); // "11021"
//    //加链表
//    //头,可以用模板
//    ListNode result = new ListNode(-1);
//    for (int i = 0; i < reverse.length(); i++) {
//        String s1 = reverse.charAt(i) + ""; // 反转之后拿出第一个"1"
//        int ge = Integer.valueOf(s1);
//        //加链表
//        addList(result, ge);
//    }
//    return result.next;
//}
//
////链表元素添加
//public void addList(ListNode head, int data) {
//    ListNode headCp = head;
//    while (head != null && head.next != null) {
//        head = head.next;
//    }
//    head.next = new ListNode(data);
//    head = headCp;
//}