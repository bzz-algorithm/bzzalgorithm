package simple;

import java.math.BigDecimal;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/11 17:39
 */
public class No172_阶乘后的零 {
    public static void main(String[] args) {
        Solution172 solution172 = new Solution172();
        int trailingZeroes = solution172.trailingZeroes(25);
        System.out.println(trailingZeroes);
    }
}

class Solution172 {
    public int trailingZeroes(int n) {
        // n/5+n/25+n/125
        //分析
        //int chu = 5;
        //int count = 0;
        //while (n / chu != 0) {
        //    count += n / chu;
        //    chu *= 5; //溢出
        //}
        //return count;

        // n/5+n/25+n/125
        // n/5 + n/5/5 + n/5/5/5 ...
        int count = 0;
        while (n > 0) {
            count += n / 5;
            n = n / 5;
        }
        return count;
    }
}


    //public int trailingZeroes(int n) {
    //    //暴力法:超出时间限制
    //    BigDecimal data = getT(n);
    //    //转字符串
    //    String s = data + "";
    //    int length = s.length();
    //    //统计0个数
    //    int count = 0;
    //    //倒序遍历
    //    for (int i = length - 1; i >= 0; i--) {
    //        char c = s.charAt(i); //获取最后0
    //        if ("0".equals(c + "")) {
    //            count++;
    //        } else {
    //            break;
    //        }
    //    }
    //    return count;
    //}
    //
    ////获取阶乘
    //public BigDecimal getT(int n) {
    //    BigDecimal bigDecimal = new BigDecimal(1);
    //    for (int i = 2; i <= n; i++) {
    //        bigDecimal = bigDecimal.multiply(new BigDecimal(i));
    //    }
    //    return bigDecimal;
    //}