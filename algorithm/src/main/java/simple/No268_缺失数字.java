package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/8/9 18:51
 */
public class No268_缺失数字 {
    public static void main(String[] args) {
        Solution268 solution268 = new Solution268();
        int[] nums = new int[]{0, 1, 4, 7, 6, 2, 5};
        int i = solution268.missingNumber(nums);
        System.out.println(i); //3
    }
}

class Solution268 {
    public int missingNumber(int[] nums) {
        //位运算(异或运算)
        //经过分析,初始化长度为初始化数字
        int miss = nums.length;
        for (int i = 0; i < nums.length; i++) {
            //对下标和对应的数字进行疯狂异或
            int yihuo = i ^ nums[i];
            miss = miss ^ yihuo;
        }
        return miss;
    }
}



    //public int missingNumber(int[] nums) {
    //    //等差数列
    //    int max = Integer.MIN_VALUE;//获取n
    //    //数据总和
    //    int sum = 0;
    //    //等差数列和
    //    int dcSum = 0;
    //    boolean zero = false;
    //    for (int i = 0; i < nums.length; i++) {
    //        //获取zero在不在
    //        if (nums[i] == 0) {
    //            zero = true;
    //        }
    //        //获取max
    //        max = Math.max(max, nums[i]);
    //        //获取sum
    //        sum += nums[i];
    //    }
    //
    //    //等差和
    //    dcSum = (1 + max) * max / 2;
    //    if (dcSum - sum == 0) {
    //        //判断0是不是在数组
    //        if (zero) {
    //            return max + 1;
    //        } else {
    //            return 0;
    //        }
    //    } else {
    //        return dcSum - sum;
    //    }
    //
    //}
