package simple;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/4/22 22:27
 */
public class No66_加一 {
    public static void main(String[] args) {
        Solution66 solution66 = new Solution66();
        int[] digits = new int[]{9,9,9,9}; // 19999 -> 20000
        int[] result = solution66.plusOne(digits);
        for (int a : result) {
            System.out.println(a);
        }
    }
}

class Solution66 {
    public int[] plusOne(int[] digits) {//1,9,9,9,9
        //进位法
        int length = digits.length; //4
        //适用于9999+1这种情况
        int[] result = new int[length + 1];
        int ge = -1;
        int jin = -1;
        for (int i = length - 1; i >= 0; i--) {
            if (i == length - 1) {
                ge = (digits[i] + 1) % 10;
                jin = (digits[i] + 1) > 9 ? 1 : 0;
            } else {
                ge = (digits[i] + jin) % 10;
                jin = (digits[i] + jin) > 9 ? 1 : 0;
            }
            digits[i] = ge;
        }
        //另外情况考虑
        if (jin == 1) {
            //加result
            result[0] = 1;
            // result [0,0,0,0,0] digits[0,0,0,0]
            for (int i = 0; i <= length - 1; i++) {
                result[i + 1] = digits[i];
            }
            return result;
        }
        return digits;
    }
}

    //public int[] plusOne(int[] digits) {//1,9,9,9,9
    //    //BigDecimal -> No2_1_3:两数相加
    //    int length = digits.length;
    //    StringBuffer sb = new StringBuffer();
    //    for (int i = 0; i < length; i++) {
    //        sb.append(digits[i]);
    //    }
    //
    //    //sb:"19999" -> 19999:BigDecimal
    //    BigDecimal bigDecimal = new BigDecimal(sb.toString());
    //
    //    //add1 add: 20000-> "20000"
    //    BigDecimal add = bigDecimal.add(new BigDecimal(1));
    //
    //    //19999+1=20000   99999+1 = 100000
    //    String strStr = add.toString(); // "20000"
    //    int strLength = strStr.length();
    //    int[] result = new int[strLength];
    //    //变成数组
    //    for (int i = 0; i < strLength; i++) {
    //        result[i] = strStr.charAt(i) - 48; //'0':48 '1':49
    //    }
    //    return result;
    //}
