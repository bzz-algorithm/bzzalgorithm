package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/29 20:10
 */
public class No168_Excel表列名称 {
    public static void main(String[] args) {
        Solution168 solution168 = new Solution168();
        String s = solution168.convertToTitle(2704);
        System.out.println(s);
    }
}

class Solution168 {
    public String convertToTitle(int n) {
        if (n <= 26) {
            return (char) (n + 65 - 1) + "";
        }
        StringBuffer sb = new StringBuffer();
        while (n != 0) {
            int yu = n % 26;
            //判断余数
            if (yu == 0) {
                yu = 26;
                //高位借1
                n = n / 26 - 1;
            } else {
                n = n / 26;
            }
            String s = (char) (yu + 65 - 1) + "";
            sb.append(s);
        }
        return sb.reverse().toString();
    }
}