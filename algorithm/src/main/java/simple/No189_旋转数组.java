package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/11 22:12
 */
public class No189_旋转数组 {
    public static void main(String[] args) {
        Solution189 solution189 = new Solution189();
        int[] nums = new int[]{1,2,3,4,5,6};
        System.out.println();
    }
}

class Solution189 {
    public void rotate(int[] nums, int k) {
        //缩小范围
        k = k % nums.length;
        //全反转部分反转
        //1.全部反转
        reverse(nums, 0, nums.length - 1);
        //2.前k个反转
        reverse(nums, 0, k - 1);
        //3.k+1个之后反转
        reverse(nums, k, nums.length - 1);
    }

    //red-green一排数据反转
    public void reverse(int[] nums, int red, int green) {
        while (red < green) {
            //red和green指针位置两数交换
            int tmp = nums[red];
            nums[red] = nums[green];
            nums[green] = tmp;
            //指针移动
            red++;
            green--;
        }
    }
}


    //public void rotate(int[] nums, int k) {
    //    //环形替换
    //    int length = nums.length;
    //    k = k % length;
    //    int checkCount = 0;
    //    for (int i = 0; i < k; i++) {
    //        int start = i;
    //        int tmp = nums[start];
    //        while (true) {
    //            //交换
    //            start = (start + k) % length;
    //            checkCount++;
    //            int thisTmp = nums[start];
    //            nums[start] = tmp;
    //            tmp = thisTmp;
    //            if (start == i) {
    //                break;
    //            }
    //        }
    //        if (checkCount == length) {
    //            //说明全部走完
    //            break;
    //        }
    //    }
    //
    //}


    //public void rotate(int[] nums, int k) {
    //    //取模法
    //    int length = nums.length;
    //    int[] newNums = new int[length];
    //    for (int i = 0; i < nums.length; i++) {
    //        newNums[(i + k) % length] = nums[i];
    //    }
    //    //新数组替换nums
    //    for (int i = 0; i < newNums.length; i++) {
    //        nums[i] = newNums[i];
    //    }
    //}


    //public void rotate(int[] nums, int k) {
    //    //暴力旋转
    //    for (int i = 1; i <= k; i++) {
    //        one(nums);
    //    }
    //}
    //
    ////给nums旋转一次
    //public void one(int[] nums) {
    //    int length = nums.length;
    //    int wa = nums[length - 1];
    //    //倒序遍历
    //    for (int i = length - 1; i > 0; i--) {
    //        nums[i] = nums[i - 1];
    //    }
    //    nums[0] = wa;
    //}