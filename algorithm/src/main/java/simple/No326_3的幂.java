package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/9/20 12:28
 */
public class No326_3的幂 {
    public static void main(String[] args) {
        Solution326 solution326 = new Solution326();
        boolean powerOfThree = solution326.isPowerOfThree(27);
        System.out.println(powerOfThree);
    }
}



class Solution326 {
    public boolean isPowerOfThree(int n) {
        return n > 0 && 1162261467 % n == 0;
    }
}


    //public boolean isPowerOfThree(int n) {
    //    //利用java API转进制
    //    // 46-> "1201"
    //    String res = Integer.toString(n, 3);//1201
    //    //1000000000000
    //    String regex = "^10*";
    //    return res.matches(regex);
    //}

    //public boolean isPowerOfThree(int n) {
    //    if (n == 0) {
    //        return false;
    //    }
    //    //短除法
    //    while (n != 1) {
    //        int yu = n % 3;
    //        n = n / 3;
    //        if (yu != 0) {
    //            return false;
    //        }
    //    }
    //    return true;
    //}

