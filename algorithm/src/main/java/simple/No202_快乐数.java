package simple;

import java.util.HashSet;
import java.util.Set;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/19 21:39
 */
public class No202_快乐数 {
    public static void main(String[] args) {
        Solution202 solution202 = new Solution202();
        boolean happy = solution202.isHappy(10);
        System.out.println(happy);
    }
}

class Solution202 {
    public boolean isHappy(int n) {
        //快慢指针
        int fast = getP(n);
        int slow = n;
        while (fast != 1) {
            fast = getP(getP(fast));
            slow = getP(slow);
            if (fast == slow) {
                return false;
            }
        }
        return true;
    }

    public int getP(int n) {
        int res = 0;
        while (n != 0) {
            //获取个位
            int ge = n % 10;
            res += ge * ge;
            n /= 10;
        }
        return res;
    }
}


    //public boolean isHappy(int n) {
    //    //暴力法
    //    Set<Integer> set = new HashSet<>();
    //    while (n != 1) {
    //        n = getP(n);
    //        if (set.contains(n)) {
    //            return false;
    //        }
    //        set.add(n);
    //    }
    //    return true;
    //}
    //
    //public int getP(int n) {
    //    int res = 0;
    //    while (n != 0) {
    //        //获取个位
    //        int ge = n % 10;
    //        res += ge * ge;
    //        n /= 10;
    //    }
    //    return res;
    //}
