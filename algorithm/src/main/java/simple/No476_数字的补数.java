package simple;

import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/3/28 12:09
 */
public class No476_数字的补数 {
    public static void main(String[] args) {
        Solution476 solution476 = new Solution476();
        int complement = solution476.findComplement(0);
        System.out.println(complement);
    }
}

class Solution476 {
    public int findComplement(int num) {
        int ge = num == 0 ? 1 : (int) (Math.log(num) / Math.log(2) + 1);
        int base = (int) (Math.pow(2, ge) - 1);
        return ~num & base;
    }
}



    //public int findComplement(int num) {
    //    int data = num;
    //    if (num == 0) {
    //        return 0;
    //    }
    //    int lowBit = -934759;
    //    //获取lowBit
    //    while (num != 0) {
    //        lowBit = num & (-num);
    //        num = num - lowBit;
    //    }
    //
    //    //num为0,说明lowBit已经是本
    //    //获取base
    //    int base = lowBit + lowBit - 1;
    //    return ~data & base;
    //}



    //public int findComplement(int num) {
    //    //用于位运算获取最高位是不是1
    //    int base = 1 << 31;
    //    int z = 0;
    //    while ((num & base) == 0) {
    //        //说明最高位是0,记录位移次
    //        z++;
    //        num = num << 1;
    //    }
    //    //当遇到最高位不是0,这时候z就是位移的次数
    //    //将num取反
    //    num = ~num;
    //    //右移获取结果
    //    return num >> z;
    //}







