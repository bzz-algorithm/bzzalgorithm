package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/2/5 23:07
 */
public class No434_字符串中的单词数 {
    public static void main(String[] args) {
        Solution434 solution434 = new Solution434();
        int i = solution434.countSegments("收到了了 啊dlds a asdladdslj  sdafl lajsls我家里收到 lkdj l");
        System.out.println(i);

    }
}

class Solution434 {
    public int countSegments(String s) {
        // 收到了了 啊dlds a asdladdslj  sdafl lajsls我家里收到 lkdj l -> 8

        //去除前后空格
        String data = s.trim();

        int res = 0;
        for (int i = 0; i < data.length(); i++) {
            if (i == 0 || (data.charAt(i) != ' ' && data.charAt(i - 1) == ' ')) {
                res++;
            }
        }
        return res;
    }

}





