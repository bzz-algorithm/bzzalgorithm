package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/11 13:13
 */
public class No171_Excel表列序号 {
    public static void main(String[] args) {
        Solution171 solution171 = new Solution171();
        String s = "HELLO";
        int titleToNumber = solution171.titleToNumber(s);
        System.out.println(titleToNumber);
    }
}


class Solution171 {
    public int titleToNumber(String s) {
        //s:CZF C:2 Z:1 F:0 指数=长度-1-i
        int sum = 0;
        int length = s.length();
        for (int i = 0; i <= length - 1; i++) {
            char c = s.charAt(i); // 'C' ->3
            int d = c - 64; //3
            sum += Math.pow(26, length - 1 - i) * d;
        }
        return sum;
    }
}