package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/11/4 22:52
 */
public class No374_猜数字大小 {
    public static void main(String[] args) {
        Solution374 solution374 = new Solution374();
        int i = solution374.guessNumber(10);
        System.out.println(i);
    }
}

class Solution374 {
    public int guessNumber(int n) {
        //二分查找
        //系统破坏器????
        int red = 0;
        int green = Integer.MAX_VALUE;
        //不正经算法!!!!
        int mid = -93475797;
        while (red < green) {
            mid = (red + green) >>> 1;
            if (guess(mid) == -1) {
                green = mid - 1;
            } else if (guess(mid) == 1) {
                red = mid + 1;
            } else {
                return mid;
            }
        }
        return red;
    }

    public int guess(int num){
        return num < 7213 ? 1 : -1;
    }
}