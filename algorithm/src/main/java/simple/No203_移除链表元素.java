package simple;

import data.ListNode;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/20 20:22
 */
public class No203_移除链表元素 {
    public static void main(String[] args) {
        Solution203 solution203 = new Solution203();
        ListNode data = new ListNode(1);
        data.add(2);
        data.add(6);
        data.add(3);
        data.add(4);
        data.add(5);
        data.add(6);
        solution203.removeElements(data, 6);

    }
}

class Solution203 {
    public ListNode removeElements(ListNode head, int val) {
        //双指针
        //头法
        ListNode red = new ListNode(-1);
        red.next = head;
        //头保存
        ListNode headCp = red;
        ListNode green = red.next;
        if (green == null) {
            return null;
        }
        while (green != null) {
            if (green.val == val) {
                //删除
                red.next = green.next;
                green = green.next;
            } else {
                //指针移动
                green = green.next;
                red = red.next;
            }
        }
        //刷回去
        red = headCp;
        return red.next;
    }
}


    //public ListNode removeElements(ListNode head, int val) {
    //    //暴力法
    //    //头法
    //    ListNode res = new ListNode(-1);
    //    while (head != null) {
    //        int data = head.val;
    //        if (data != val) {
    //            //加元素到res
    //            addList(res, data);
    //        }
    //        //指针移动
    //        head = head.next;
    //    }
    //    return res.next;
    //}
    //
    //
    //public void addList(ListNode head, int data) {
    //    //保存
    //    ListNode headCp = head;
    //    //模板
    //    while (head != null && head.next != null) {
    //        head = head.next;
    //    }
    //    head.next = new ListNode(data);
    //    //刷回去
    //    head = headCp;
    //}