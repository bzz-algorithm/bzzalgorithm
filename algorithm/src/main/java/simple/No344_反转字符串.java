package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/10/7 18:34
 */
public class No344_反转字符串 {
    public static void main(String[] args) {
        Solution344 solution344 = new Solution344();
        char[] s = new char[]{'a', 'b', 'c', 'd', 'e'};
        solution344.reverseString(s);
    }
}

class Solution344 {
    // O(1)空间复杂度
    public void reverseString(char[] s) {
        if (s == null || s.length == 0) {
            return;
        }
        //s长度是5的时候,循环3次 0,1,2
        //s长度是6的时候,循环3次 0,1,2
        int red = 0;
        int green = s.length - 1;
        int length = s.length;

        int count = (length - 1) / 2;
        for (int i = 0; i <= count; i++) {
            //开始交换
            char tmp = s[red];
            s[red] = s[green];
            s[green] = tmp;
            red++;
            green--;
        }
    }
}
