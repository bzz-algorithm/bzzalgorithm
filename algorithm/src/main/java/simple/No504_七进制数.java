package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/4/29 19:56
 */
public class No504_七进制数 {
    public static void main(String[] args) {
        Solution504 solution504 = new Solution504();
        String s = solution504.convertToBase7(-7959);
        System.out.println(s);
    }    
}

class Solution504 {
    String res = "";
    public String convertToBase7(int num) {
        //递归终止条件
        if (num == 0) {
            return "0";
        } else if (num < 0) {
            num = -num;
            res += "-";
        }

        int yu = num % 7; //0 3 
        //1137
        //162
        convertToBase7(num / 7);
        res += yu;
        //递归法
        return res;
    }
}



    //public String convertToBase7(int num) {
    //    StringBuffer res = new StringBuffer();
    //    //短除法
    //    if (num < 0) {
    //        num = -num;
    //        res.append("-");
    //    } else if (num == 0) {
    //        return "0";
    //    }
    //    while (num != 0) {
    //        int yu = num % 7;
    //        res.append(yu);
    //        num = num / 7;
    //    }
    //    
    //    //注意负数问题
    //    //当前res:-03123
    //    res.reverse(); //res=32130-
    //    if (res.substring(res.length() - 1).equals("-")) {
    //        // -
    //        String a = res.substring(res.length() - 1);
    //        String b = res.substring(0, res.length() - 1);
    //        return a + b;
    //    } else {
    //        return res.toString();
    //    } 
    //}






