package simple;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/5/5 13:13
 */
public class No506_相对名次 {
    public static void main(String[] args) {
        Solution506 solution506 = new Solution506();
        
        //[4,2,3,5,1]
        int[] score = new int[]{32, 78, 49, 11, 89};
        String[] relativeRanks = solution506.findRelativeRanks(score);
        System.out.println(relativeRanks);
        
    }
}

class Solution506 {
    public String[] findRelativeRanks(int[] score) {
        // "Gold Medal", "Silver Medal", "Bronze Medal"
        //使用哈希存放索引
        String[] rank = new String[score.length];
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < score.length; i++) {
            //元素,索引
            map.put(score[i], i);
        }

        //[11,32,49,78,89]
        Arrays.sort(score);

        for (int i = 0; i < score.length; i++) {
            //排名
            int sort = score.length - i;
            //获取原本位置
            int index = map.get(score[i]);
            //对应位置放对应排名,处理排名
            if (sort == 1) {
                rank[index] = "Gold Medal";
            } else if (sort == 2) {
                rank[index] = "Silver Medal";
            } else if (sort == 3) {
                rank[index] = "Bronze Medal";
            } else {
                //其余全是排名
                rank[index] = sort + "";
            } 
        }
        return rank;
    }
}



