package simple;

import java.util.Arrays;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/2/12 15:37
 */
public class No455_分发饼干 {
    public static void main(String[] args) {
        Solution455 solution455 = new Solution455();
        int[] g = new int[]{19, 1, 17};
        int[] s = new int[]{16, 12, 18};
        int children = solution455.findContentChildren(g, s);
        System.out.println(children);
    }
}

class Solution455 {
    public int findContentChildren(int[] g, int[] s) {
        //19    1     17
        //16    12    18

        //排序法
        Arrays.sort(g);
        Arrays.sort(s);

        //孩子数:满足
        int count = 0;
        //饼干索引
        int index = 0;
        //遍历孩子群
        for (int i = 0; i < g.length; i++) {
            while (index < s.length) {
                //孩子满足,饼干跳过,孩子跳过
                if (s[index] >= g[i]) {
                    count++;
                    index++;
                    break;
                }
                //不满足,饼干跳过
                index++;
            }
        }
        return count;
    }
}




