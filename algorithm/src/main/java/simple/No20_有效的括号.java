package simple;

import java.util.*;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/20 21:38
 */
public class No20_有效的括号 {
    public static void main(String[] args) {
        Solution20 solution20 = new Solution20();
        String s = "([{}])";
        boolean valid = solution20.isValid(s);
        System.out.println();
    }
}

class Solution20 {
    public boolean isValid(String s) {
        //栈法
        Map<Character, Character> map = new HashMap<>();
        map.put('}', '{');
        map.put(']', '[');
        map.put(')', '(');
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char w王子 = s.charAt(i);
            if (stack.empty() || map.containsValue(w王子)) {
                stack.push(w王子);
            } else {
                //右开
                Character check = stack.pop();
                System.out.println();
                if (!(map.get(w王子) == check)) {
                    return false;
                }
            }
        }
        return stack.empty();
    }
}

    //public boolean isValid(String s) {
    //    //连连看,王子找公主!
    //    Map<Character, Character> map = new HashMap<>();
    //    map.put('}', '{');
    //    map.put(']', '[');
    //    map.put(')', '(');
    //    int red = -1;
    //    List<Character> list = new ArrayList<>();
    //    for (int i = 0; i < s.length(); i++) {
    //        char 王子 = s.charAt(i);
    //        if (list.size() == 0) {
    //            list.add(王子);
    //            red++;
    //        } else if (map.containsValue(王子)) {
    //            list.add(王子);
    //            red++;
    //        } else if (map.containsKey(王子)) {
    //            if (list.get(red) == map.get(王子)) {
    //                list.remove(list.size() - 1);
    //                red--;
    //            } else {
    //                return false;
    //            }
    //        }
    //    }
    //    return list.size() == 0;
    //}
