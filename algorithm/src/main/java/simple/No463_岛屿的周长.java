package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/3/14 21:25
 */
public class No463_岛屿的周长 {
    public static void main(String[] args) {
        Solution463 solution463 = new Solution463();
        int[][] data = new int[][]{{1,0}};
        solution463.islandPerimeter(data);
    }

}

class Solution463 {
    public int islandPerimeter(int[][] grid) {
        // 口口口  // 1 1 1
        // 口⊙⊙  // 1 0 0
        // ⊙⊙⊙  // 0 0 0
        
        // 迷宫回溯
        int leftLength = grid[0].length;
        int upLength = grid.length;
        int c = 0;

        for (int left = 0; left < leftLength; left++) {
            for (int up = 0; up < upLength; up++) {
                //岛屿,才递归
                if (grid[up][left] == 1) {
                    c += getC(grid, up, left);  
                }
            }
        }
        return c;
    }

    //递归函数,返回周长
    public int getC(int[][] grid, int up, int left) {
        //边缘条件判断
        if (up < 0 || left < 0 || up == grid.length || left == grid[0].length || grid[up][left] == 0) {
            return 1;
        }
        //-7959:代表走过
        if (grid[up][left] == -7959) {
            return 0;
        }
        grid[up][left] = -7959;
        //上
        return getC(grid, up - 1, left) +
                //左
                getC(grid, up, left - 1) +
                //下
                getC(grid, up + 1, left) +
                //右
                getC(grid, up, left + 1);
        
    }
}



    //public int islandPerimeter(int[][] grid) {
    //    // 口口口  // 1 1 1
    //    // 口⊙⊙  // 1 0 0
    //    // ⊙⊙⊙  // 0 0 0
    //    int leftLength = grid[0].length;
    //    int upLength = grid.length;
    //    //岛屿周长
    //    int c = 0;
    //    for (int up = 0; up < upLength; up++) {
    //        for (int left = 0; left < leftLength; left++) {
    //            //岛屿才计算
    //            //判断周围水域情况
    //            if (grid[up][left] == 1) {
    //                //上
    //                if (up - 1 < 0 || grid[up - 1][left] == 0) {
    //                    c += 1;
    //                }
    //                //下
    //                if (up + 1 == upLength || grid[up + 1][left] == 0) {
    //                    c += 1;
    //                }
    //                //左
    //                if (left - 1 < 0 || grid[up][left - 1] == 0) {
    //                    c += 1;
    //                }
    //                //右
    //                if (left + 1 == leftLength || grid[up][left + 1] == 0) {
    //                    c += 1;
    //                }
    //            }
    //        }
    //    }
    //    return c;
    //}



    //public int islandPerimeter(int[][] grid) {
    //    // 口口口  // 1 1 1
    //    // 口⊙⊙  // 1 0 0
    //    // ⊙⊙⊙  // 0 0 0
    //    int leftLength = grid[0].length;
    //    int upLength = grid.length;
    //    //岛屿周长
    //    int c = 0;
    //    for (int left = 0; left < leftLength; left++) {
    //        for (int up = 0; up < upLength; up++) {
    //            if (grid[up][left] == 1) {
    //                c += 4;
    //                //判断左边和上边
    //                if (left - 1 >= 0 && grid[up][left - 1] == 1) {
    //                    c -= 2;
    //                }
    //
    //                if (up - 1 >= 0 && grid[up - 1][left] == 1) {
    //                    c -= 2;
    //                }
    //            }
    //
    //        }
    //    }
    //    return c;
    //}
