package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/5/2 17:29
 */
public class No69_x的平方根 {
    public static void main(String[] args) {
        Solution69 solution69 = new Solution69();
        int i = solution69.mySqrt(8);
        System.out.println(i);
    }
}


class Solution69 {
    public static int mySqrt(int x) {
        //牛顿法
        double test = 6666;//随便一个
        while (true) {
            double a = test;
            test = (test + x / test) / 2;
            //目的是为了跟a比较,如果距离小于1,则可以直接返回
            double b = test;
            if (Math.abs(a - b) < 1) {
                return (int) test;
            }
        }
    }
}

    //public static int mySqrt(int x) {
    //    //资深api调用
    //    return (int) Math.pow(Math.pow(Math.E, 0.5),Math.log(x));
    //}


    //public static int mySqrt(int x) {
    //    if (x == 0 || x == 1) {
    //        return x;
    //    }
    //    //伪二分
    //    int red = 0;
    //    int green = x;
    //    int mid = -45793479;//初始化
    //    while (green - red != 1) {
    //        mid = (red + green) / 2; // 72
    //        int check = x / mid;
    //        if (mid > check) {
    //            green = mid;
    //        } else if (mid < check) {
    //            red = mid;
    //        } else {
    //            return mid;
    //        }
    //    }
    //    return (red + green) / 2;
    //}

    //public static int mySqrt(int x) {
    //    //暴力法
    //    //if (x == 0) {
    //    //    return 0;
    //    //}
    //
    //    //78 8*8=64
    //    //78 9*9=81
    //    for (long i = 1; i <= x; i++) {
    //        if (i * i <= x && (i + 1) * (i + 1) > x) {
    //            return (int)i;
    //        }
    //    }
    //    return -0;
    //}