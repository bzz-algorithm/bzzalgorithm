package simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/14 20:52
 */
public class No13_罗马数字转整数 {
    public static void main(String[] args) {
        Solution13 solution13 = new Solution13();
        String s = "LVIII";
        int result = solution13.romanToInt(s);
        System.out.println();
    }
}

class Solution13 {
    public int romanToInt(String s) {
        //穷举法,考验机器性能
        //获取所有罗马数字的对应map
        Map<String, Integer> map = new HashMap<>();
        for (int i = 1; i <= 3999; i++) {
            getResult(map, i);
        }
        return map.get(s);
    }

    //将x整型数字转罗马并加入allData集合
    public void getResult(Map<String, Integer> allData, int x) {
        Map<Integer, String> luomaMap = init();
        // 2876 = 2000 + 800 + 70 + 6
        int ge = x % 10; // 6
        int shi = x % 100 - ge; // 70
        int bai = x % 1000 - shi - ge; // 800
        int qian = x / 1000 * 1000; // 2000

        // 2000
        StringBuffer key = new StringBuffer();
        key.append(luomaMap.get(qian))
                .append(luomaMap.get(bai))
                .append(luomaMap.get(shi))
                .append(luomaMap.get(ge));
        allData.put(key.toString().replaceAll("null", ""), x);
    }


    public Map<Integer, String> init() {
        Map<Integer, String> map = new HashMap<>();
        //1-9
        map.put(1, "I");
        map.put(2, "II");
        map.put(3, "III");
        map.put(4, "IV");
        map.put(5, "V");
        map.put(6, "VI");
        map.put(7, "VII");
        map.put(8, "VIII");
        map.put(9, "IX");

        //10-90
        map.put(10, "X");
        map.put(20, "XX");
        map.put(30, "XXX");
        map.put(40, "XL");
        map.put(50, "L");
        map.put(60, "LX");
        map.put(70, "LXX");
        map.put(80, "LXXX");
        map.put(90, "XC");

        //100-900
        map.put(100, "C");
        map.put(200, "CC");
        map.put(300, "CCC");
        map.put(400, "CD");
        map.put(500, "D");
        map.put(600, "DC");
        map.put(700, "DCC");
        map.put(800, "DCCC");
        map.put(900, "CM");


        //1000-3000
        map.put(1000, "M");
        map.put(2000, "MM");
        map.put(3000, "MMM");
        return map;
    }

}

//--------------------------
//    public int romanToInt(String s) {
//        Map<String, Integer> checkMap = init();
//        if (s.length() == 1) {
//            return checkMap.get(s);
//        }
//        //长度大于1
//        int green = 0;
//        int result = 0;
//        while (green < s.length()) {
//            boolean flag = false;
//            String one = s.substring(green, green + 1);
//            String two = green == s.length() - 1 ?
//                    s.substring(green) : s.substring(green, green + 2);
//            if (checkMap.get(two) != null) {
//                result += checkMap.get(two);
//                flag = true;
//            } else {
//                result += checkMap.get(one);
//            }
//            if (flag) {
//                green += 2;
//            } else {
//                green += 1;
//            }
//        }
//        return result;
//    }
//
//    public Map<String, Integer> init() {
//        Map<String, Integer> map = new HashMap<>();
//        map.put("I", 1);
//        map.put("V", 5);
//        map.put("X", 10);
//        map.put("L", 50);
//        map.put("C", 100);
//        map.put("D", 500);
//        map.put("M", 1000);
//
//        map.put("IV", 4);
//        map.put("IX", 9);
//        map.put("XL", 40);
//        map.put("XC", 90);
//        map.put("CD", 400);
//        map.put("CM", 900);
//        return map;
//    }
//------------------------