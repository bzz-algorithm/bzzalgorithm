package simple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/8/8 21:15
 */
public class No242_有效的字母异位词 {
    public static void main(String[] args) {
        Solution242 solution242 = new Solution242();
        boolean anagram = solution242.isAnagram("a", "b");
        System.out.println(anagram);
    }
}

class Solution242 {
    public boolean isAnagram(String s, String t) {
        //哈希映射
        //anagram  a->3 n->1 g->1 r->1 m->1
        //angaraa
        //(字母,个数)
        Map<Character, Integer> map = new HashMap<>();
        //初始化
        for (int i = 0; i < s.length(); i++) {
            //当前字母
            char c = s.charAt(i);
            addMapKeys(map, c);
        }

        //遍历t
        for (int i = 0; i < t.length(); i++) {
            char c = t.charAt(i);
            if (map.get(c) == null) {
                return false;
            } else {
                subMapKeys(map, c);
            }
        }

        //最后遍历map,看值怎么样,有没有不是0的
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            int v = entry.getValue();
            if (v != 0) {
                return false;
            }
        }
        return true;
    }

    //对c加1
    public void addMapKeys(Map<Character, Integer> map, char c) {
        if (map.get(c) == null) {
            map.put(c, 1);
        } else {
            map.put(c, map.get(c) + 1);
        }
    }

    //对c-1
    public void subMapKeys(Map<Character, Integer> map, char c) {
        if (map.get(c) == null) {
            return;
        } else {
            map.put(c, map.get(c) - 1);
        }
    }
}


    //public boolean isAnagram(String s, String t) {
    //    if (s.length() != t.length()) {
    //        return false;
    //    }
    //    //s:anagram
    //    //t:nagaram
    //    //排序
    //    //car   cra  rca cat  acr    act
    //    char[] charS = s.toCharArray();
    //    char[] charT = t.toCharArray();
    //    Arrays.sort(charS);
    //    Arrays.sort(charT);
    //    int length = charS.length;
    //    boolean flag = true;
    //    for (int i = 0; i < length; i++) {
    //        if (charS[i] != charT[i]) {
    //            flag = false;
    //            break;
    //        }
    //    }
    //    return flag;
    //}