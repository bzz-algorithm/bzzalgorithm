package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/19 12:20
 */
public class No198_打家劫舍 {
    public static void main(String[] args) {
        Solution198 solution198 = new Solution198();
        int[] nums = new int[]{5, 7, 6, 8, 63, 6, 3, 2, 2};
        int rob = solution198.rob(nums);
        System.out.println(rob);
    }
}

class Solution198 {
    public int rob(int[] nums) {
        if (nums.length == 0) {
            return 0;
        } else if (nums.length == 1) {
            return nums[0];
        }
        //算法进阶:动态规划(O(1))
        int length = nums.length;
        int a = nums[0];
        int b = nums[0] > nums[1] ? nums[0] : nums[1];
        for (int i = 2; i < length; i++) {
            //当前i-1个
            int tmp = a;
            a = b;
            b = Math.max(tmp + nums[i], a);
        }
        return b;
    }
}


    //public int rob(int[] nums) {
    //    if (nums.length == 0) {
    //        return 0;
    //    } else if (nums.length == 1) {
    //        return nums[0];
    //    } else if (nums.length == 2) {
    //        return nums[0] > nums[1] ? nums[0] : nums[1];
    //
    //    }
    //    //算法进阶:动态规划
    //    int length = nums.length;
    //    int[] dp = new int[length];
    //    dp[0] = nums[0];
    //    dp[1] = nums[0] > nums[1] ? nums[0] : nums[1];
    //    for (int i = 2; i < length; i++) {
    //        //选择偷
    //        int a = dp[i - 2] + nums[i];
    //        //不偷
    //        int b = dp[i - 1];
    //        //当前i最大金额
    //        dp[i] = Math.max(a, b);
    //    }
    //    return dp[length - 1];
    //}