package simple;

import java.math.BigInteger;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/4/26 23:03
 */
public class No67_二进制求和 {
    public static void main(String[] args) {
        Solution67 solution67 = new Solution67();
        // 874   "1101101010"
        // 29366 "111001010110110"
        // 30240 "111011000100000"
        String s = solution67.addBinary("1101101010", "111001010110110");
        System.out.println(s);

    }
}

class Solution67 {
    public String addBinary(String a, String b) {
        //位运算
        BigInteger aBig = new BigInteger(a, 2);
        BigInteger bBig = new BigInteger(b, 2);
        BigInteger sum = aBig.xor(bBig);
        BigInteger sign = aBig.and(bBig).shiftLeft(1); //左移1
        BigInteger zero = new BigInteger("0", 2);

        while (sign.compareTo(zero) != 0) {
            sum = aBig.xor(bBig);
            sign = aBig.and(bBig).shiftLeft(1);
            aBig = sum;
            bBig = sign;
        }
        return sum.toString(2);//转二进制的string
    }
}

    //public String addBinary(String a, String b) {
    //    //进位添数法
    //    int aLength = a.length();
    //    int bLength = b.length();
    //    int delta = -1;//初始化差值
    //    delta = Math.abs(aLength - bLength);
    //
    //    //补0
    //    if (aLength < b.length()) {
    //        //补a
    //        for (int i = 1; i <= delta; i++) {
    //            a = "0" + a;
    //        }
    //    } else if (a.length() > b.length()) {
    //        //补b
    //        for (int i = 1; i <= delta; i++) {
    //            b = "0" + b;
    //        }
    //    }
    //
    //    // 0011XX
    //    // 111010
    //
    //    //取b.length也可以
    //    int length = a.length();
    //    int ge = 0;
    //    int jin = 0;
    //    StringBuffer result = new StringBuffer();
    //    for (int i = length - 1; i >= 0; i--) {
    //        int aGe = a.charAt(i) - 48;
    //        int bGe = b.charAt(i) - 48;
    //        ge = (aGe + bGe + jin) % 2;
    //        jin = aGe + bGe + jin > 1 ? 1 : 0;
    //        result.append(ge);
    //    }
    //    //循环结束,大于1情况考虑
    //    if (jin == 1) {
    //        result.append(1);
    //    }
    //
    //    return result.reverse().toString();
    //}


    //public String addBinary(String a, String b) {
    //    //强行硬写+BigDecimal 进阶
    //    BigDecimal add = getO(a).add(getO(b));
    //    //十进制转二进制
    //    String result = getB(add);
    //    return result;
    //}
    //
    //
    //////二进制转十进制
    ////public BigDecimal getO(String binaryStr) {
    ////    int length = binaryStr.length();
    ////    BigDecimal cheng = new BigDecimal(1);
    ////    BigDecimal result = new BigDecimal(0);
    ////    for (int i = length - 1; i >= 0; i--) {
    ////        int num = binaryStr.charAt(i) - 48;
    ////        result = result.add(cheng.multiply(new BigDecimal(num)));
    ////        cheng = cheng.multiply(new BigDecimal(2));
    ////    }
    ////    return result;
    ////}
    ////
    //////十进制转二进制
    ////public String getB(BigDecimal obj) {
    ////    StringBuffer sb = new StringBuffer(); //""
    ////    //检查是不是0,如果是0,后面循环直接退出
    ////    BigDecimal check = new BigDecimal(0);
    ////    BigDecimal base = new BigDecimal(2);
    ////    if (obj.subtract(check).toString().equals("0")) {
    ////        return "0";
    ////    }
    ////    while (!obj.subtract(check).toString().equals("0")) {
    ////        //43
    ////        //如何获取1?
    ////        sb.append(obj.subtract(obj.divideToIntegralValue(base).multiply(base)));
    ////        obj = obj.divideToIntegralValue(base);
    ////    }
    ////    return sb.reverse().toString();
    ////}
