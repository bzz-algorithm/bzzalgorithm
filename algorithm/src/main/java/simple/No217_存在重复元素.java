package simple;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/30 23:26
 */
public class No217_存在重复元素 {
    public static void main(String[] args) {
        Solution217 solution217 = new Solution217();
        int[] nums = new int[]{1, 4, 45, 6565, 43, 45, 35, 43};
        boolean b = solution217.containsDuplicate(nums);
        System.out.println(b);
    }
}

class Solution217 {
    public boolean containsDuplicate(int[] nums) {
        //set去重
        //1,2,4,3,1 -> nums.length = 5
        //set:1,2,3,4 set.size = 4

        Set<Integer> set = new HashSet<>();

        for (int num : nums) {
            set.add(num);
        }
        return set.size() != nums.length;
    }
}
