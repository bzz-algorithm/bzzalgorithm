package simple;

import javafx.util.Pair;

import java.util.*;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/9/12 9:47
 */
public class No290_单词规律 {
    public static void main(String[] args) {
        Solution290 solution290 = new Solution290();
        boolean b = solution290.wordPattern("abc", "dog dog dog");
        System.out.println(b);
    }
}

class Solution290 {
    public boolean wordPattern(String pattern, String s) {
        //暴力遍历
        if (pattern == null || s == null || pattern.length() != s.split(" ").length) {
            return false;
        }
        int red = 0;
        int green = 0;
        String[] data = s.split(" ");
        //维护一个map
        Map<Character, String> map = new HashMap<>();
        while (red < data.length) {
            char c = pattern.charAt(red);
            String str = data[green];
            if (map.get(c) == null) {
                if (map.containsValue(str) && !map.containsKey(c)) {
                    return false;
                }
                map.put(c, str);
            } else {
                if (!map.get(c).equals(str)) {
                    return false;
                }
            }
            red++;
            green++;
        }
        return true;
    }
}





    //public boolean wordPattern(String pattern, String s) {
    //    //双向映射
    //    if (pattern == null || s == null || pattern.length() != s.split(" ").length) {
    //        return false;
    //    }
    //
    //    int red = 0;
    //    int green = 0;
    //    String[] data = s.split(" ");
    //
    //    //双向Map
    //    Map<Character, String> map1 = new HashMap<>();
    //    Map<String , Character> map2 = new HashMap<>();
    //    while (red < data.length) {
    //        char c = pattern.charAt(red);
    //        String str = data[green];
    //        if (map1.get(c) == null) {
    //            map1.put(c, str);
    //            //map2处理
    //            if (map2.get(str) == null) {
    //                map2.put(str, c);
    //            } else {
    //                return false;//注意逻辑
    //            }
    //        } else {
    //            if (!map1.get(c).equals(str)) {
    //                return false;
    //            }
    //        }
    //        red++;
    //        green++;
    //
    //    }
    //    return true;
    //}

