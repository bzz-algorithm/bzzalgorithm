package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/4/4 23:29
 */
public class No485_最大连续1的个数 {
    public static void main(String[] args) {
        Solution485 solution485 = new Solution485();
        int[] nums = new int[]{1,0,1};
        int maxConsecutiveOnes = solution485.findMaxConsecutiveOnes(nums);
        System.out.println(maxConsecutiveOnes);
    }
}

class Solution485 {
    public int findMaxConsecutiveOnes(int[] nums) {
        //字符串搞出
        String 拉大锯 = "";
        //遍历nums,获取拉大锯
        for (int 老规 : nums) {
            拉大锯 += 老规;
        }
        //110011110111
        //正则匹配
        
        //11绿豆糕1111绿豆糕111
        拉大锯 = 拉大锯.replaceAll("0+", "绿豆糕");
        //切分
        String[] 阿里觉 = 拉大锯.split("绿豆糕");
        //获取最长值
        int 了让 = 0;
        for (String 起床气 : 阿里觉) {
            //11,1111,111
            //拿最大值
            了让 = Math.max(了让, 起床气.length());
        }
        return 了让;
    }
}



    //public int findMaxConsecutiveOnes(int[] nums) {
    //    //清零并计数
    //    //总进度
    //    int length = nums.length;
    //    //指针
    //    int index = 0;
    //    //计数器
    //    int count = 0;
    //    //最大连续1的个数
    //    int result = 0;
    //    while (index < length) {
    //        if (nums[index] == 1) {
    //            count++;
    //        } else {
    //            result = Math.max(result, count);
    //            count = 0;
    //        }
    //        index++;
    //    }
    //    //再来一步,解决全是1的数据
    //    result = Math.max(result, count);
    //    return result;
    //}



    //public int findMaxConsecutiveOnes(int[] nums) {
    //    //双指针
    //    //总进度
    //    int length = nums.length;
    //    int red = 0;
    //    int green = 0;
    //    int result = 0;
    //    while (green < length) {
    //        //总进度
    //        while (green<length && nums[green] == 1) {
    //            //速度超出问题
    //            green++;
    //        }
    //        //1->0
    //        //记录1的个数
    //        result = Math.max(result, green - red);
    //        while (green < length && nums[green] == 0) {
    //            green++;
    //        }
    //        //0->1
    //        //下一次基准
    //        red = green;
    //    }
    //    //进度结束
    //    return result;
    //}






