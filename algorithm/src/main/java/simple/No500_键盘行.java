package simple;

import java.util.*;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/4/22 20:09
 */
public class No500_键盘行 {
    public static void main(String[] args) {
        Solution500 solution500 = new Solution500();
        String[] words = new String[]{"bus", "room", "two", "lag"};
        String[] words1 = solution500.findWords(words);
        System.out.println();
    }
}

class Solution500 {
    public String[] findWords(String[] words) {
        //asdf -> 2 asdf
        // jslgjlasdfljlrwortuofjlgaks ->   2,2,1 false
        
        //暴力法
        //遍历
        Map<Character, Integer> checkMap = initMap();
        Set<Integer> set = new HashSet<>();
        //用于辅助
        List<String> list = new ArrayList<>();
        
        out:for (String word : words) {
            set.clear();
            for (char w : word.toLowerCase().toCharArray()) {
                //b,u,s
                int base = checkMap.get(w);
                set.add(base);
                if (set.size() != 1) {
                    //说明在多个行,false
                    continue out;
                }
            }
            list.add(word);
        }
        
        //结果出
        String[] res = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }
    
    public Map<Character, Integer> initMap() {
        Map<Character, Integer> map = new HashMap<>();
        
        //第一行
        map.put('q', 1);
        map.put('w', 1);
        map.put('e', 1);
        map.put('r', 1);
        map.put('t', 1);
        map.put('y', 1);
        map.put('u', 1);
        map.put('i', 1);
        map.put('o', 1);
        map.put('p', 1);
        
        //第二行
        map.put('a', 2);
        map.put('s', 2);
        map.put('d', 2);
        map.put('f', 2);
        map.put('g', 2);
        map.put('h', 2);
        map.put('j', 2);
        map.put('k', 2);
        map.put('l', 2);
        
        //第三行
        map.put('z', 3);
        map.put('x', 3);
        map.put('c', 3);
        map.put('v', 3);
        map.put('b', 3);
        map.put('n', 3);
        map.put('m', 3);
        return map;
    }
}






