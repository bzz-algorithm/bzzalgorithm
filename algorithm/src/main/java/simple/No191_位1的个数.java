package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/18 19:10
 */
public class No191_位1的个数 {
    public static void main(String[] args) {
        Solution191 solution191 = new Solution191();
        int i = solution191.hammingWeight(82);
        System.out.println(i);
    }
}

class Solution191 {
    public int hammingWeight(int n) {
        //二进制位运算(模拟十进制加减)
        n = ((n & 0x55555555)) + ((n >> 1) & 0x55555555); // 0101
        n = ((n & 0x33333333)) + ((n >> 2) & 0x33333333); // 0011
        n = ((n & 0x0f0f0f0f)) + ((n >> 4) & 0x0f0f0f0f); // 00001111
        n = ((n & 0x00ff00ff)) + ((n >> 8) & 0x00ff00ff); // 0000000011111111
        n = ((n & 0x0000ffff)) + ((n >> 16) & 0x0000ffff); // 0101
        return n;
    }
}

    //public int hammingWeight(int n) {
    //    //借1与1
    //    int count = 0;
    //    while (n != 0) {
    //        int check = n - 1;
    //        n = n & check;
    //        count++;
    //    }
    //    return count;
    //}

    //public int hammingWeight(int n) {
    //    //位运算
    //    //判断是不是1,拿到记录数
    //    int count = 0;
    //    for (int i = 0; i <= 31; i++) {
    //        //逐位数字检查
    //        int check = (n >> i) & 1;
    //        if (check == 1) {
    //            count++;
    //        }
    //    }
    //    return count;
    //}