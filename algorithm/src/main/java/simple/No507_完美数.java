package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/5/6 20:38
 */
public class No507_完美数 {
    public static void main(String[] args) {
        Solution507 solution507 = new Solution507();
        boolean b = solution507.checkPerfectNumber(28);
        System.out.println(b);
    }
}

class Solution507 {
    public boolean checkPerfectNumber(int num) {
        //欧拉定理
        //遍历n:1-20
        //2^20-1 1024 * 1024 -1 > 1000000
        //2^19 > 500000 > 100000
        //2^20-1 * 2^19 > 1E11 > Integer.MAX_VALUE
        for (int n = 1; n <= 20; n++) {
            int ern_1 = (int) (Math.pow(2, n) - 1);
            //2^(n-1)
            int a = (int) Math.pow(2, n - 1);
            int check = a * ern_1;
            if (isPrime(ern_1)) {
                if (num == check) {
                    return true;
                }
            }
        }
        return false;
    }

    //素数判断代码
    public boolean isPrime(int ern_1) {
        if (ern_1 == 1) {
            return false;
        }
        for (int i = 2; i * i <= ern_1; i++) {
            if (ern_1 % i == 0) {
                return false;
            }
        }
        return true;
    }
}



    //public boolean checkPerfectNumber(int num) {
    //    //改良版暴力遍历
    //    int check = 1;
    //    //考虑如果num是1情况
    //    if (check == num) {
    //        return false;
    //    }
    //    for (int i = 2; i * i <= num; i++) {
    //        if (num % i == 0) {
    //            //整除可以
    //            check += i; 
    //            //拿到另外一个因素:因子
    //            check += num / i;
    //        }
    //    }
    //    return num == check;
    //}



    //public boolean checkPerfectNumber(int num) {
    //    //玩具,暴力遍历
    //    //记录所有因子之和
    //    int check = 0;
    //    //遍历所有可能因子
    //    //1-num/2
    //    for (int i = 1; i <= num / 2; i++) {
    //        if (num % i == 0) {
    //            check += i;
    //        }
    //    }
    //    return check == num;
    //}






