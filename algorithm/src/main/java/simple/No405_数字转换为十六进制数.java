package simple;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/12/27 21:53
 */
public class No405_数字转换为十六进制数 {
    public static void main(String[] args) {
        Solution405 solution405 = new Solution405();
        String s = solution405.toHex(16);
        System.out.println(s);
    }
}

class Solution405 {
    public String toHex(int num) {
        if (num == 0) {
            return "0";
        }
        //能不用栈吗??
        //做两个辅助
        int base = -1 << 28;
        int check = 15;

        Map<Integer, String> map = init();
        //标记最多移动次数
        int count = 1;
        while ("0".equals(getHighHex(num, map, base, check))) {
            num = num << 4;
            count++;
        }
        //说明开始最高位不是0了
        //获取值
        //这时候的num:0001 1010 0000000....
        StringBuffer sb = new StringBuffer();
        while (count <= 8) {
            String v = getHighHex(num, map, base, check);
            sb.append(v);
            num = num << 4;
            count++;
        }
        return sb.toString();
    }

    //定义一个方法,获取最高位4个组成的16进制代码
    public String getHighHex(int num, Map<Integer, String> map, int base, int check) {
        int a = num & base; //1011 0000000(28个0)
        int b = a >> 28; //0000000(28个0)1011
        //获取1011对应代码
        int key = b & check;
        return map.get(key);
    }

    //1-15->16进制表达式
    public Map<Integer, String> init() {
        Map<Integer, String> map = new HashMap<>();
        map.put(0, "0");
        map.put(1, "1");
        map.put(2, "2");
        map.put(3, "3");
        map.put(4, "4");
        map.put(5, "5");
        map.put(6, "6");
        map.put(7, "7");
        map.put(8, "8");
        map.put(9, "9");
        map.put(10, "a");
        map.put(11, "b");
        map.put(12, "c");
        map.put(13, "d");
        map.put(14, "e");
        map.put(15, "f");
        return map;
    }
}



    //public String toHex(int num) {
    //    if (num == 0) {
    //        return "0";
    //    }
    //    //确定基准
    //    int base = 15;
    //    //注意到-1 -> ffffffff
    //    //定义右移次数
    //    int count = 1;
    //    Map<Integer, String> map = init();
    //    //与的结果,与map进行匹配获取对应 16进制数/字母
    //    int key = -4574959;
    //
    //    Stack<String> stack = new Stack<>();
    //    while (num != 0 && count <= 8) {
    //        //13
    //        key = num & base;
    //        //d
    //        String value = map.get(key);
    //        //扔栈
    //        stack.push(value);
    //        num = num >> 4;
    //        count++;
    //    }
    //
    //    //stack数据拿出
    //    StringBuffer sb = new StringBuffer();
    //    while (!stack.isEmpty()) {
    //        String pop = stack.pop();
    //        sb.append(pop);
    //    }
    //    return sb.toString();
    //}
    //
    ////1-15->16进制表达式
    //public Map<Integer, String> init() {
    //    Map<Integer, String> map = new HashMap<>();
    //    map.put(0, "0");
    //    map.put(1, "1");
    //    map.put(2, "2");
    //    map.put(3, "3");
    //    map.put(4, "4");
    //    map.put(5, "5");
    //    map.put(6, "6");
    //    map.put(7, "7");
    //    map.put(8, "8");
    //    map.put(9, "9");
    //    map.put(10, "a");
    //    map.put(11, "b");
    //    map.put(12, "c");
    //    map.put(13, "d");
    //    map.put(14, "e");
    //    map.put(15, "f");
    //    return map;
    //}
