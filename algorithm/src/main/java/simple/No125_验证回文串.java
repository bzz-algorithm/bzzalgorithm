package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/21 23:48
 */
public class No125_验证回文串 {
    public static void main(String[] args) {
        Solution125 solution125 = new Solution125();
        //boolean res = solution125.isPalindrome("MA,$^%$ %a fC,-`CF  *a-  .A-M");
        boolean res = solution125.isPalindrome("race a car");
        System.out.println(res);
    }
}

class Solution125 {
    public boolean isPalindrome(String s) {
        if (s.length() == 0) {
            return true;
        }
        //维护指针
        int red = 0;
        int green = s.length() - 1;
        String small = s.toLowerCase();
        char a = ' ';
        char b = '\u0001';
        while (red < green) {
            a = small.charAt(red);
            b = small.charAt(green);
            while (!isaz09(a)) {
                //跳过
                red++;
                if (red == green) {
                    return true;
                }
                a = small.charAt(red);
            }

            //绿指针处理
            while (!isaz09(b)) {
                //跳过
                green--;
                if (red == green) {
                    return true;
                }
                b = small.charAt(green);
            }

            if (a == b) {
                red++;
                green--;
            } else {
                return false;
            }
        }
        return true;
    }

    public boolean isaz09(char c) {
        return (c >= 48 && c <= 57) || (c >= 97 && c <= 122);
    }
}

    //public boolean isPalindrome(String s) {
    //    //第一步,转小写
    //    String data = s.toLowerCase();
    //    //第二步
    //    data = data.replaceAll("[^0-9a-z]", "");
    //    //第三步
    //    StringBuffer sb = new StringBuffer();
    //    String reverseStr = sb.append(data).reverse().toString();
    //    return reverseStr.equals(data);
    //}