package simple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/25 11:23
 */
public class No136_只出现一次的数字 {
    public static void main(String[] args) {
        Solution136 solution136 = new Solution136();
        int[] nums = new int[]{7, 7, 9, 2, 8, 3, 9, 2, 3};
        int one = solution136.singleNumber(nums);
        System.out.println(one);
    }
}

class Solution136 {
    public int singleNumber(int[] nums) {
        //按位异或
        // 1 1 = 0, 0 0 = 0 01=1,10=1
        //A ^ A = 0
        //0 ^ A = A
        int res = 0;
        for (int num : nums) {
            res = res ^ num;
        }
        return res;
    }
}

    //public int singleNumber(int[] nums) {
    //    //哈希映射
    //    Map<Integer, Integer> map = new HashMap<>();
    //    for (int num : nums) {
    //        addMapKeys(map, num);
    //    }
    //    //map初始化完毕
    //    //遍历
    //    for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
    //        if (entry.getValue() == 1) {
    //            return entry.getKey();
    //        }
    //    }
    //    return 34057 - 34;
    //}
    //
    //public void addMapKeys(Map<Integer, Integer> map, int key) {
    //    if (map.get(key) == null) {
    //        map.put(key, 1);
    //    } else {
    //        map.put(key, map.get(key) + 1);
    //    }
    //}

    //public int singleNumber(int[] nums) {
    //    Arrays.sort(nums);
    //    //双指针
    //    int length = nums.length;
    //    //-1:为了青色指针判断用
    //    for (int i = 0; i < nums.length - 1; i += 2) {
    //        //对数判断,是不是一对
    //        if (nums[i] != nums[i + 1]) {
    //            return nums[i];
    //        }
    //    }
    //    return nums[length - 1];
    //}

    //public int singleNumber(int[] nums) {
    //    //暴力法
    //    for (int i = 0; i < nums.length; i++) {
    //        int check = nums[i];
    //        boolean flag = false;//代表没有重复
    //        for (int j = 0; j < nums.length; j++) {
    //            //比较与i不同位置的其他数
    //            if (i != j) {
    //                if (check == nums[j]) {
    //                    //找到了,置为true
    //                    flag = true;
    //                    break;
    //                }
    //            }
    //        }
    //        //观察flag,如果false,则返回
    //        if (!flag) {
    //            return nums[i];
    //        }
    //
    //    }
    //    return 8305349;
    //}
