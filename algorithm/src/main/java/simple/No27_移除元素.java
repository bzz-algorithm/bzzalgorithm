package simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/28 17:47
 */
public class No27_移除元素 {
    public static void main(String[] args) {
        Solution27 solution27 = new Solution27();
        // 0,4,7,1,1,8 顺序无所谓
        int[] nums = new int[]{0, 2, 4, 2, 7, 1, 1, 2, 8};
        int val = 2;
        int i = solution27.removeElement(nums, val);
        System.out.println();
    }
}

class Solution27 {
    public int removeElement(int[] nums, int val) {
        //双指针,最终优化版
        int red = 0;
        for (int green = 0; green <= nums.length - 1; green++) {
            if (nums[green] != val) {
                nums[red] = nums[green];
                red++;
            }
        }
        return red;
    }
}

    //public int removeElement(int[] nums, int val) {
    //    //双指针
    //    int red = 0;
    //    int yellow = nums.length - 1;
    //    while (red <= yellow) {
    //        if (nums[red] == val) {
    //            //删除,同时yellow-1
    //            remove(red, nums, yellow);
    //            yellow--;
    //        } else {
    //            red++;
    //        }
    //    }
    //    return yellow + 1;
    //}
    //
    //public void remove(int red, int[] nums, int yellow) {
    //    for (int i = red; i <= yellow - 1; i++) {
    //        nums[i] = nums[i + 1];
    //    }
    //}

    //public int removeElement(int[] nums, int val) {
    //    //鸡贼法,以结果为导向
    //    List<Integer> list = new ArrayList<>();
    //    for (int num : nums) {
    //        if (val != num) {
    //            list.add(num);
    //        }
    //    }
    //
    //    //刷回去,欺骗系统
    //    for (int i = 0; i < list.size(); i++) {
    //        nums[i] = list.get(i);
    //    }
    //    return list.size();
    //}
