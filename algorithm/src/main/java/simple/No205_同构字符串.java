package simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/24 19:23
 */
public class No205_同构字符串 {
    public static void main(String[] args) {
        Solution205 solution205 = new Solution205();
        boolean isomorphic = solution205.isIsomorphic("aee", "abc");
        System.out.println(isomorphic);
    }
}


class Solution205 {
    public boolean isIsomorphic(String s, String t) {
        //自增法
        //aee cff
        //122 122
        //ajsdlfj
        //1234562
        return getZ(s).equals(getZ(t));
    }

    //data-> 自增字符串形如1223141等
    public String getZ(String data) {
        Map<Character, Integer> map = new HashMap<>();
        int length = data.length();
        //维护自增变量
        int count = 0;
        //自增字符串
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            char c = data.charAt(i);
            if (map.get(c) == null) {
                //(a,1)
                map.put(c, ++count);
            } else {
                sb.append(map.get(c));
            }
        }
        return sb.toString();
    }
}



    //public boolean isIsomorphic(String s, String t) {
    //    //哈希映射
    //    if (s.length() != t.length()) {
    //        return false;
    //    }
    //    int length = s.length();
    //    Map<Character, Character> map = new HashMap<>();
    //    for (int i = 0; i < length; i++) {
    //        char a = s.charAt(i);
    //        char b = t.charAt(i);
    //        //开始判断
    //        //说明前面已经有另一半了
    //        if (map.get(a) != null && map.get(a) != b) {
    //            return false;
    //        } else if (map.get(a) == null && map.containsValue(b)) {
    //            return false;
    //        } else if (map.get(a) == null) {
    //            map.put(a, b);
    //        }
    //    }
    //    return true;
    //}