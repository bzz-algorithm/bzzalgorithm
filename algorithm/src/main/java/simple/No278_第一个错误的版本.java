package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/8/9 21:31
 */
public class No278_第一个错误的版本 {
    public static void main(String[] args) {
        Solution278 solution278 = new Solution278();
        int i = solution278.firstBadVersion(10000);
        System.out.println(i);
    }
}

class Solution278 extends VersionControl278{
    public int firstBadVersion(int n) {
        //二分查找
        int left = 0;
        int right = n;
        int mid = 0;
        while (left <= right) {
            mid = left + (right - left) / 2;
            if (isBadVersion(mid)) {
                //true,移动右
                right = mid - 1;
            } else {
                //false,移动左
                left = mid + 1;
            }
        }
        //left > right
        // (n + n+1 ) /2 = n + 0.5 = n
        if (isBadVersion(mid)) {
            return mid;
        } else {
            return mid + 1;
        }
    }
}

class VersionControl278 {
    public boolean isBadVersion(int version) {
        //第7659个之后都是坏的
        return version >= 7659;
    }
}
