package simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/4/20 23:32
 */
public class No58_最后一个单词的长度 {
    public static void main(String[] args) {
        Solution58 solution58 = new Solution58();
        //仅包含大小写字母和空格 ' '
        String s = "STOP THE WHOLE AIADFAD";
        int result = solution58.lengthOfLastWord(s);
        System.out.println();
    }
}

class Solution58 {
    public int lengthOfLastWord(String s) {
        if(s.equals("")) return 0;
        int green = s.length() - 1; //考虑到length 为0的问题
        //green>=0
        //获取第一个green的位置(不能为空)
        while (green >= 0 && s.charAt(green) == ' ') {
            green--;
        }
        //green一定指向不是空的位置
        //计算单词长度
        int count = 0;
        while (green >= 0 && s.charAt(green) != ' ') {
            count++;
            green--;
        }
        return count;
    }
}


    //public int lengthOfLastWord(String s) {
    //    //api调用
    //    //Stop The Whole Wared
    //    //api再次调用
    //    try {
    //        String[] strs = s.split(" ");
    //        int length = strs.length; //4
    //        int na = length - 1;
    //        return strs[na].length();
    //    } catch (Exception e) {
    //        return 0;
    //    }
    //}

