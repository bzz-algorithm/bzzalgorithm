package simple;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/1/28 19:43
 */
public class No412_FizzBuzz {
    public static void main(String[] args) {
        Solution412 solution412 = new Solution412();
        List<String> list = solution412.fizzBuzz(15);
        System.out.println();
    }
}

class Solution412 {
    public List<String> fizzBuzz(int n) {
        //存放3,5,7,11等整除数
        List<String> keyList = new ArrayList<>();
        keyList.add("3_Fizz");
        keyList.add("5_Buzz");
        //keyList.add("7_7777");
        //keyList.add("11_1111");

        List<String> res = new ArrayList<>();

        //对每一个i进行遍历(i(1->n))
        for (int i = 1; i <= n; i++) {
            //对每一个i再遍历keyList里面的整除数
            //对于每一个i,定义一个listStr
            String listStr = "";
            //i=385  3 5 7 11
            for (String data : keyList) {
                //3 Buzz  5 Fizz
                Integer key = Integer.valueOf(data.split("_")[0]);
                String value = data.split("_")[1];
                if (i % key == 0) {
                    listStr += value;
                }
            }

            //for结束之后,判断listStr
            if ("".equals(listStr)) {
                //说明皆不能整除
                res.add(i + "");
            } else {
                res.add(listStr);
            }
        }
        return res;
    }
}
