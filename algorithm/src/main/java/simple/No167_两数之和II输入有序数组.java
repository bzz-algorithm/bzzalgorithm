package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/27 20:42
 */
public class No167_两数之和II输入有序数组 {
    public static void main(String[] args) {
        Solution167 solution167 = new Solution167();
        int[] numbers = new int[]{5, 8, 13, 17, 22, 31};
        int[] result = solution167.twoSum(numbers, 25);
        System.out.println();
    }
}

class Solution167 {
    public int[] twoSum(int[] numbers, int target) {
        //双指针
        //5,8,13,17,22,31
        int red = 0;
        int green = numbers.length - 1;
        while (red < green) {
            int check = numbers[red] + numbers[green];
            if (check > target) {
                //调整绿指针
                green--;
            } else if (check < target) {
                //调整红指针
                red++;
            } else {
                return new int[]{red + 1, green + 1};
            }
        }
        return null;
    }
}
