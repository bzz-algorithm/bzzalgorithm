package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/11 22:28
 */
public class No7_整数反转 {
    public static void main(String[] args) {
        Solution7 solution7 = new Solution7();
        int x = -1147665439;
        //-2147483648  2147483647
        // 2147483647 + 1 = -2147483648
        int reverse = solution7.reverse(x); // 102842
        System.out.println();
    }
}

class Solution7 {
    public int reverse(int x) { //248201000
        // 指针进位
        int result = 0;
        int ge = 0;
        //21474836 -> check -> Integer.MAX
        //214748364 * 10 + 7
        //-214748364 * 10 + <-8
        int check = Integer.MAX_VALUE / 10;
        while (x != 0) {
            ge = x % 10; //0
            if (result > check || (result == check && ge > 7)) {
                return 0;
            }
            if (result < -check || (result == check && ge < -8)) {
                return 0;
            }
            result = result * 10 + ge;
            //指针前移
            x = x / 10;
        }
        return result;
    }
}

    //public int reverse(int x) {
    //    try {
    //        //投机取巧
    //        int fu = 0;
    //        fu = x > 0 ? 1 : -1;
    //        //去符号,方便后面计算
    //        int data = Math.abs(x);
    //        //转字符串
    //        String s = data + "";
    //        StringBuffer sb = new StringBuffer();
    //        String reverseStr = sb.append(s).reverse().toString(); // "000248201"
    //
    //        //去掉前面的0
    //        for (int i = 0; i < reverseStr.length(); i++) {
    //            if (!(reverseStr.charAt(i) == '0')) {
    //                reverseStr = reverseStr.substring(i);
    //                break;
    //
    //            }
    //        }
    //        //reverseStr "248201"
    //        switch (fu) {
    //            case -1:
    //                return -Integer.valueOf(reverseStr);
    //            case 1:
    //                return Integer.valueOf(reverseStr);
    //        }
    //
    //
    //    } catch (Exception e) {
    //        return 0;
    //    }
    //    return 0;
    //
    //}