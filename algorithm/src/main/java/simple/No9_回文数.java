package simple;

import java.math.BigDecimal;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/13 21:53
 */
public class No9_回文数 {
    public static void main(String[] args) {
        Solution9 solution9 = new Solution9();
        int x = 123554321;
        boolean result = solution9.isPalindrome(x);
        System.out.println();
    }
}

class Solution9 {
    public boolean isPalindrome(int x) {
        // 取半反转比较对数法

        //数据源 123454321 奇数个
        // x            rNumx
        // 12345432  1
        // 1234543   12
        // 123454    123
        // 12345     1234
        // 1234      12345 x == rNumx / 10

        //偶数个: 12334321
        // 1233432   1
        // 123343   12
        // 12334   123
        // 1233   1234  x == rNumx

        if (x < 0 || (x != 0 && x % 10 == 0)) {
            return false;
        }
        int rNumx = 0;
        while (x > rNumx) {
            int ge = x % 10;
            rNumx = rNumx * 10 + ge;
            x = x / 10;
        }
        return x == rNumx || x == rNumx / 10;

    }
}


    //public boolean isPalindrome(int x) {
    //    //双指针合作法
    //    if (x < 0) {
    //        return false;
    //    }
    //
    //    String data = x + "";
    //    int length = data.length(); // 9
    //    int red = length - 1;
    //    int green = 0;
    //    int count = length / 2; // 4
    //    for (int i = 0; i < count; i++) {
    //        char a = data.charAt(green);
    //        char b = data.charAt(red);
    //        if (a != b) {
    //            return false;
    //        }
    //        red--;
    //        green++;
    //    }
    //    return true;
    //}

    //public boolean isPalindrome(int x) {
    //    //1.赖皮
    //    //究极赖皮,大数之间相加减
    //    if (x < 0) {
    //        return false;
    //    }
    //
    //    //超级赖皮
    //    StringBuffer sb = new StringBuffer();
    //    StringBuffer reverseSb = new StringBuffer();
    //
    //    String str = sb.append(x).toString();
    //    String reverseStr = reverseSb.append(x).reverse().toString();
    //
    //    //Bigdecimal
    //    BigDecimal xBigDecimal = new BigDecimal(str);
    //    BigDecimal reverseBigDecimal = new BigDecimal(reverseStr);
    //
    //    BigDecimal result = xBigDecimal.subtract(reverseBigDecimal); //0
    //    int i = result.intValue();
    //    return i == 0;
    //}