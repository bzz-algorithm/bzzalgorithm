package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/16 23:01
 */
public class No121_买卖股票的最佳时机 {
    public static void main(String[] args) {
        Solution121 solution121 = new Solution121();
        int[] prices = new int[]{14, 18, 24, 7, 12, 17, 4, 16};
        int res = solution121.maxProfit(prices);
        System.out.println(res);
    }
}

class Solution121 {
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        //随着遍历而变化的最小值
        int minCheck = prices[0];
        //最大利润
        int maxMoney = 0;
        //一次遍历
        for (int i = 0; i < prices.length; i++) {
            minCheck = Math.min(prices[i], minCheck);
            if (prices[i] > minCheck) {
                //说明有利润
                int money = prices[i] - minCheck;
                maxMoney = Math.max(maxMoney, money);
            }
        }
        return maxMoney;
    }
}


    //public int maxProfit(int[] prices) {
    //    //暴力法
    //    //14, 18, 24, 7, 12, 17, 4, 16
    //    int maxMoney = 0;
    //    int lenth = prices.length;
    //    for (int i = 0; i < prices.length; i++) { //代表第i天买入
    //        //卖出必须大于i天位置
    //        for (int j = i + 1; j < prices.length; j++) {
    //            int money = prices[j] - prices[i];
    //            maxMoney = Math.max(maxMoney, money);
    //        }
    //    }
    //    return maxMoney;
    //}
