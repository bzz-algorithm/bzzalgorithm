package simple;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/10/7 20:48
 */
public class No345_反转字符串中的元音字母 {
    public static void main(String[] args) {
        Solution345 solution345 = new Solution345();
        String s = "hello";
        String vowels = solution345.reverseVowels(s);
        System.out.println(vowels);
        System.out.println();
    }
}

class Solution345 {
    public String reverseVowels(String s) {
        char[] data = s.toCharArray();
        List<Pair<Integer, Character>> list = new ArrayList<>();
        List<Pair<Integer, Character>> listR = new ArrayList<>();

        //初始化
        for (int i = 0; i < data.length; i++) {
            char c = data[i];
            if(isY(c)){
                list.add(new Pair<>(i, c));
            }
        }

        //listR初始化
        //反转list
        for (int i = list.size() - 1; i >= 0; i--) {
            listR.add(list.get(i));
        }

        //list和listR进行关联
        for (int i = 0; i < list.size(); i++) {
            Pair<Integer, Character> pair = list.get(i);
            Pair<Integer, Character> pairR = listR.get(i);

            //获取索引
            int index = pair.getKey();
            data[index] = pairR.getValue();
        }

        //拼回去
        StringBuffer sb = new StringBuffer();
        for (char c : data) {
            sb.append(c + "");
        }
        return sb.toString();

    }


    //判断元音
    public boolean isY(char c) {
        return "a".equals((c+"").toLowerCase()) ||
                "e".equals((c+"").toLowerCase()) ||
                "i".equals((c+"").toLowerCase()) ||
                "o".equals((c+"").toLowerCase()) ||
                "u".equals((c+"").toLowerCase());
    }
}

    //// O(1)空间复杂度
    //public String reverseVowels(String s) {
    //    //xxxx1xx2xx34
    //    //xxxx4xx3xx21
    //    int length = s.length();
    //    int red = 0;
    //    int green = length - 1;
    //    char[] data = s.toCharArray();
    //    while (red <= green) {
    //        //red指针操作
    //        //考虑大小写问题
    //        //不断循环找下一个元音
    //        while (red <= green && !"a".equals((data[red] + "").toLowerCase()) &&
    //                !"e".equals((data[red] + "").toLowerCase()) &&
    //                !"i".equals((data[red] + "").toLowerCase()) &&
    //                !"o".equals((data[red] + "").toLowerCase()) &&
    //                !"u".equals((data[red] + "").toLowerCase())) {
    //            red ++;
    //        }
    //
    //        //为了无元音退出循环
    //        if (red > green) {
    //            break;
    //        }
    //
    //        //green指针操作
    //        while (red <= green && !"a".equals((data[green] + "").toLowerCase()) &&
    //                !"e".equals((data[green] + "").toLowerCase()) &&
    //                !"i".equals((data[green] + "").toLowerCase()) &&
    //                !"o".equals((data[green] + "").toLowerCase()) &&
    //                !"u".equals((data[green] + "").toLowerCase())) {
    //            green --;
    //        }
    //
    //        if (red > green) {
    //            break;
    //        }
    //        //开始交换
    //        char tmp = data[red];
    //        data[red] = data[green];
    //        data[green] = tmp;
    //
    //        //单元音情况退出循环
    //        red++;
    //        green--;
    //    }
    //    StringBuffer sb = new StringBuffer();
    //    for (char c : data) {
    //        sb.append(c + "");
    //    }
    //    return sb.toString();
    //
    //}