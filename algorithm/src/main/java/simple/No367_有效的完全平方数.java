package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/10/29 21:07
 */
public class No367_有效的完全平方数 {
    public static void main(String[] args) {
        Solution367 solution367 = new Solution367();
        boolean perfectSquare = solution367.isPerfectSquare(2147395600);
        System.out.println(perfectSquare);
    }
}

class Solution367 {
    public boolean isPerfectSquare(int num) {
        //数学公式
        //等差数列求和

        //时间复杂度:根号num

        int jian = 1;
        while (num > 0) {
            //减数
            num = num - jian;
            //减数+2
            jian = jian + 2;
        }
        return num == 0;
    }
}


    //public boolean isPerfectSquare(int num) {
    //    //牛顿迭代法
    //    //No69.x的平方根
    //    double A = num;
    //    double preA = num;
    //    double data = num;
    //    while (true) {
    //        A = (preA + data / preA) / 2;
    //        if (preA - A < 1) {
    //            int a = (int) preA;
    //            int b = (int) A;
    //            return a * a == num || b * b == num;
    //        }
    //        preA = A;
    //    }
    //}


    //public boolean isPerfectSquare(int num) {
    //    //65536 256
    //    //4 2
    //    //5 ???? false
    //
    //    //暴力
    //    //65536  1->65536
    //    //534085048604 - -
    //
    //    //二分查找
    //    int left = 1;
    //    int right = num;
    //    int mid = -1;
    //    while (left <= right) {
    //        mid = (left + right) >>> 1;
    //        int check = num / mid;
    //        if (check < mid) {
    //            right = mid - 1;
    //        } else if (check > mid) {
    //            left = mid + 1;
    //        } else {
    //            return check * mid == num;
    //        }
    //    }
    //
    //    return false;
    //}
