package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/3/6 20:47
 */
public class No461_汉明距离 {
    public static void main(String[] args) {
        Solution461 solution461 = new Solution461();
        int diff = solution461.hammingDistance(579, 943);
        System.out.println(diff);
    }
}

class Solution461 {
    public int hammingDistance(int x, int y) {
        //不正经算法系列更新调整啦!
        //更新日期:
        //奇数月的偶数日,偶数月的奇数日

        //No191第三个方法

        //辅助二进制
        //XOR
        int z = x ^ y;

        //return get1(z);
        return Integer.bitCount(z);
    }

    //获取z中二进制1的个数
    public int get1(int z) {
        //01010101 -> 0x55
        //左组+右组
        z = ((z >> 1) & 0x55555555) + (z & 0x55555555);
        //00110011 -> 0x33
        z = ((z >> 2) & 0x33333333) + (z & 0x33333333);
        //00001111 -> 0x0f
        z = ((z >> 4) & 0x0f0f0f0f) + (z & 0x0f0f0f0f);
        //0000000011111111 -> 0x00ff
        z = ((z >> 8) & 0x00ff00ff) + (z & 0x00ff00ff);
        //0000000000000000f....
        z = ((z >> 16) & 0x0000ffff) + (z & 0x0000ffff);
        return z;
    }

}



    //public int hammingDistance(int x, int y) {
    //    //不正经算法系列更新调整啦!
    //    //更新日期:
    //    //奇数月的偶数日,偶数月的奇数日
    //
    //    //参考No191.位1的个数(借1与1)
    //
    //    int z = x ^ y;
    //    //记录1的个数
    //    int count = 0;
    //    while (z != 0) {
    //        int z1 = z - 1;
    //        z = z & z1;
    //        count++;
    //    }
    //    return count++;
    //}



    //public int hammingDistance(int x, int y) {
    //    //不正经算法系列更新调整啦!
    //    //更新日期:
    //    //奇数月的偶数日,偶数月的奇数日
    //
    //    // 异或运算 01=1,00=0 11=0
    //    // x:0101
    //    // y:0011
    //    // z:0110 -> 2个
    //
    //    //数:011011011 -> 1有几个??
    //
    //    //脚法: 6个
    //    //标记1的位置
    //    int z = x ^ y;
    //    //标记1的个数
    //    int count = 0;
    //
    //    while (z != 0) {
    //        int check = z & 1;
    //        if (check == 1) {
    //            count++;
    //        }
    //        z = z >> 1;
    //    }
    //    return count;
    //}






