package simple;

import java.util.*;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/25 20:09
 */
public class No26_删除排序数组中的重复项 {
    public static void main(String[] args) {
        Solution26 solution26 = new Solution26();
        int nums[] = {7, 9, 9, 9, 9, 17, 17, 19, 20, 20};
        int i = solution26.removeDuplicates(nums);
        System.out.println();
    }
}

class Solution26 {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 1) {
            return 1;
        }
        //快慢指针
        int red = 0;
        int green = 1;
        int length = nums.length;
        while (green <= length - 1) {
            //指针相比
            if (nums[red] != nums[green]) {
                red++;
                nums[red] = nums[green];
                green++;
            } else {
                green++;
            }
        }
        return red + 1;
    }
}

    //public int removeDuplicates(int[] nums) {
    //    //7, 9, 9, 11, 14, 17, 17, 19, 20, 20
    //    //鸡贼法
    //    Set<Integer> set = new HashSet<>();
    //    for (int num : nums) {
    //        //去重
    //        set.add(num);
    //    }
    //    List<Integer> list = new ArrayList<>();
    //    for (int num : set) {
    //        list.add(num);
    //    }
    //    //list排序
    //    list.sort(new Comparator<Integer>() {
    //        @Override
    //        public int compare(Integer o1, Integer o2) {
    //            return o1 >= o2 ? 1 : -1;
    //        }
    //    });
    //    for (int i = 0; i < list.size(); i++) {
    //        nums[i] = list.get(i);
    //    }
    //    return list.size();
    //}

    //public int removeDuplicates(int[] nums) {
    //    if (nums.length == 1) {
    //        return 1;
    //    }
    //    //双指针,加最外层指针
    //    int red = 0;
    //    int green = 1;
    //    int yellow = nums.length - 1;
    //    while (green <= yellow) {
    //        if (nums[red] == nums[green]) {
    //            //删除
    //            remove(green, nums, yellow);
    //            yellow -= 1;
    //        } else {
    //            red++;
    //            green++;
    //        }
    //    }
    //    return yellow + 1;
    //}
    //
    ////删除green位置的元素
    //public void remove(int green, int nums[], int yellow) {
    //    for (int i = green; i <= yellow - 1; i++) {
    //        nums[i] = nums[i + 1];
    //    }
    //}
