package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/2/2 20:31
 */
public class No414_第三大的数 {
    public static void main(String[] args) {
        Solution414 solution414 = new Solution414();
        int[] nums = new int[]{32, 47, 16, 45, 23, 17, 26, 34};
        int i = solution414.thirdMax(nums);
        //1:34
        System.out.println(i);

    }
}

class Solution414 {
    public int thirdMax(int[] nums) {
        //一次遍历??
        //第一,二,三大的数存放
        long one = Long.MIN_VALUE;
        long two = Long.MIN_VALUE;
        long three = Long.MIN_VALUE;

        //遍历nums
        for (int num : nums) {
            //注意重复问题
            if (num == one || num == two || num <= three) {
                continue;
            }
            //推入位置确定
            if (num > one) {
                three = two;
                two = one;
                one = num;
            } else if (num > two && num < one) {
                // one num two
                three = two;
                two = num;
            } else if (num > three && num < two) {
                three = num;
            }
        }

        //判断three是不是变过
        if (three == Long.MIN_VALUE) {
            //没变
            return (int) one;
        } else {
            return (int) three;
        }

    }
}
