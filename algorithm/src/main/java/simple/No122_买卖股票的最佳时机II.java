package simple;

import javafx.util.Pair;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/20 12:42
 */
public class No122_买卖股票的最佳时机II {
    public static void main(String[] args) {
        Solution122 solution122 = new Solution122();
        int[] prices = new int[]{14, 18, 24, 7, 12, 17, 4, 16};
        //int[] prices = new int[]{7,6,4,3,1};
        int res = solution122.maxProfit(prices);
        System.out.println(res);
    }
}

class Solution122 {
    public int maxProfit(int[] prices) {
        if (prices.length == 0 || prices.length == 1) {
            return 0;
        }
        int check = prices[0];
        int maxMoney = 0;
        //一次遍历
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > check) {
                //说明单调增,有钱拿!
                maxMoney += prices[i] - check;
            }
            check = prices[i];
        }
        return maxMoney;
    }
}


    //public int maxProfit(int[] prices) {
    //    //暴力法
    //    return getMaxMoney(prices, 0);
    //}
    //
    //public int getMaxMoney(int[] prices, int index) {
    //    int maxMoney = 0;
    //    for (int i = index; i < prices.length; i++) {
    //        for (int j = i + 1; j < prices.length; j++) {
    //            //绿指针如何带入?
    //            //何时递归?? i<j (位置的数)
    //            if (prices[i] < prices[j]) {
    //                //有钱拿!!
    //                int money = prices[j] - prices[i]
    //                        + getMaxMoney(prices, j + 1);
    //                maxMoney = Math.max(money, maxMoney);
    //            }
    //        }
    //    }
    //    return maxMoney;
    //}

