package simple;

import java.util.Stack;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/3/31 21:14
 */
public class No482_密钥格式化 {
    public static void main(String[] args) {
        Solution482 solution482 = new Solution482();
        String s = solution482.licenseKeyFormatting("--a-b-c-d--", 2);
        System.out.println(s);

    }
}

class Solution482 {
    public String licenseKeyFormatting(String S, int K) {
        //S:----
        //干掉'-'
        String data = S.replace("-", "");
        if (data.equals("")) {
            return "";
        } 
        Stack<Character> stack = new Stack<>();
        //记录扔了几个字母
        int count = 0;
        //倒序遍历
        for (int i = data.length() - 1; i >= 0; i--) {
            char c = data.charAt(i);
            //记录扔了几个字母,count
            count++;
            stack.push(c);
            //扔进K的倍数个字母,扔'-'
            if (count % K == 0) {
                stack.push('-');
            }
        }
        
        //处理'-'
        StringBuffer res = new StringBuffer();

        while (!stack.isEmpty()) {
            res.append(stack.pop());
        }

        if (res.toString().charAt(0) != '-') {
            //ab-cde-fgh
            return res.toString().toUpperCase();
        } else {
            //-abcd-efgh
            return res.toString().substring(1).toUpperCase();
        } 
    }
}



    //public String licenseKeyFormatting(String S, int K) {
    //    //S = "----"
    //    String data = S.replace("-", "");
    //    if (data.equals("")) {
    //        return "";
    //    }
    //    //8
    //    int dLength = data.length();
    //    //2
    //    int yu = dLength % K;
    //    //处理第一组
    //    StringBuffer res = new StringBuffer();
    //    if (yu != 0) {
    //        for (int i = 0; i < yu; i++) {
    //            res.append(data.charAt(i));
    //        }
    //        res.append("-");
    //    }
    //
    //    //用于当count % K ==0 的时候加"-"
    //    int count = 0;
    //    for (int i = yu; i < dLength; i++) {
    //        res.append(data.charAt(i));
    //        count++;
    //        if (count % K == 0) {
    //            res.append("-");
    //        }
    //    }
    //    return res.toString().substring(0, res.toString().length() - 1)
    //            .toUpperCase();
    //}





