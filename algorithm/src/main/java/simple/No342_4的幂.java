package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/9/20 23:15
 */
public class No342_4的幂 {
    public static void main(String[] args) {
        Solution342 solution342 = new Solution342();
        int num = 4;
        boolean powerOfFour = solution342.isPowerOfFour(num);
        System.out.println(powerOfFour);
    }
}

class Solution342 {

    //O(1)
    public boolean isPowerOfFour(int num) {
        //1.判断是2的幂
        //100000000
        //lowbit 100010001001000
        //????1000               ????1000
        //取反:  xxxx0111 + 1 -> xxxx1000 -x



        //2.判断是4的幂
        int check = 0x55555555;
        int flag = num & check;
        return num > 0 && isPowerOfTwo(num) && flag == num;
    }


    //判断2的幂
    public boolean isPowerOfTwo(int num){
        return (num & (-num)) == num;
    }
}


