package simple;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/11/6 21:01
 */
public class No383_赎金信 {
    public static void main(String[] args) {
        Solution383 solution383 = new Solution383();
        boolean b = solution383.canConstruct("aa", "aab");
        System.out.println(b);
    }
}

class Solution383 {
    public boolean canConstruct(String ransomNote, String magazine) {
        //排序
        char[] ransomNoteChar = ransomNote.toCharArray();
        char[] magazineChar = magazine.toCharArray();
        //排序
        Arrays.sort(ransomNoteChar);
        Arrays.sort(magazineChar);
        //ransomNoteChar指针
        int a = 0;
        //magazineChar指针
        int b = 0;
        while (a < ransomNoteChar.length && b < magazineChar.length) {
            if (ransomNoteChar[a] > magazineChar[b]) {
                b++;
            } else if (ransomNoteChar[a] < magazineChar[b]) {
                //说明magazineChar没有ransomNoteChar
                return false;
            } else {
                //两个指针同时移动
                a++;
                b++;
            }
        }
        return a == ransomNote.length();
    }
}

    //public boolean canConstruct(String ransomNote, String magazine) {
    //    //不使用map
    //    //字母26个
    //    int[] save = new int[26];
    //    for (char s : ransomNote.toCharArray()) {
    //        int index = magazine.indexOf(s, save[s - 'a']);
    //        if (index == -1) {
    //            return false;
    //        }
    //        //为下一个一模一样做准备
    //        save[s - 'a'] = index + 1;
    //    }
    //    return true;
    //}

    //public boolean canConstruct(String ransomNote, String magazine) {
    //    // abad   asldfjalgvhalufland
    //    // abad   asldfjalgvhbalufland
    //    //哈希
    //    //对于A:字符串,初始化
    //    Map<Character, Integer> map = new HashMap<>();
    //    //对赎金信字符串初始化map
    //    for (int i = 0; i < ransomNote.length(); i++) {
    //        char c = ransomNote.charAt(i);
    //        addMapKeys(map, c);
    //    }
    //
    //    //杂志字符串匹配赎金信字符串
    //    // abad   abadlasdjglashlfhkasdhflasdjadksgk
    //    //(a,2) (b,1) (d,1)
    //    for (int i = 0; i < magazine.length(); i++) {
    //        char c = magazine.charAt(i);
    //        //(a,2) (b,1) (d,1)
    //        if (map.get(c) == null) {
    //            continue;
    //        } else if (map.get(c) != null) {
    //            //说明拿到了,减去1
    //            map.put(c, map.get(c) - 1);
    //            //当值为0,删除
    //            if (map.get(c) == 0) {
    //                map.remove(c);
    //            }
    //        }
    //        if (map.size() == 0) {
    //            //后面不走
    //            break;
    //        }
    //    }
    //    return map.size() == 0;
    //
    //
    //}
    //
    ////初始化map
    //public void addMapKeys(Map<Character, Integer> map, char key) {
    //    if (map.get(key) == null) {
    //        map.put(key, 1);
    //    } else {
    //        map.put(key, map.get(key) + 1);
    //    }
    //}