package simple;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/6/14 19:36
 */
public class No119_杨辉三角II {
    public static void main(String[] args) {
        Solution119 solution119 = new Solution119();
        List<Integer> row = solution119.getRow(25);
        System.out.println();
    }
}

class Solution119 {
    public List<Integer> getRow(int rowIndex) {
        //数学大法
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i <= rowIndex; i++) {
            res.add(getC(rowIndex, i));
        }
        return res;
    }

    //获取C(n,k)
    public int getC(int n, int k) {
        float res = 1;
        for (int i = 1; i <= k; i++) {
            res = res * (n - i + 1) / i;
        }
        return (int) res;
    }
}


    //public List<Integer> getRow(int rowIndex) {
    //    //自己迭代自己 1331->14641
    //    List<Integer> res = new ArrayList<>();
    //    res.add(1);
    //    if (rowIndex == 0) {
    //        return res;
    //    }
    //    //初始化
    //    int green = 1;
    //
    //    //自己迭代
    //    for (int i = 1; i <= rowIndex; i++) {
    //        //假如1331->14641
    //
    //        //i=1:res=1
    //        //i=2:res=11
    //        //i=3:res=121
    //        //i=4:res=1331 -> 14641
    //        for (int j = 1; j <= i - 1; j++) {
    //            int tmp = green; //1 3 3
    //            green = res.get(j); // 3 3 1
    //            int n = tmp + green; //4 6 4
    //            res.set(j, n);
    //        }
    //        res.add(1);
    //    }
    //    return res;
    //}


    //public List<Integer> getRow(int rowIndex) {
    //    //头
    //    List<Integer> begin = new ArrayList<>();
    //    //结果
    //    List<Integer> res = new ArrayList<>();
    //    //头初始化
    //    begin.add(1);
    //    if (rowIndex == 0) {
    //        return begin;
    //    }
    //
    //    for (int i = 1; i <= rowIndex; i++) {
    //        //假如121,以例
    //        int length = begin.size(); //3 => 4
    //        //每一层都是空开始加
    //        res = new ArrayList<>();
    //        res.add(1);
    //        for (int j = 0; j <= length - 2; j++) {
    //            int n = begin.get(j) + begin.get(j + 1);// 3 3
    //            res.add(n);
    //        }
    //        res.add(1); // 1331
    //        begin = res;
    //    }
    //    return res;
    //}