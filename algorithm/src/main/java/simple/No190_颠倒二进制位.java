package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/7/12 20:45
 */
public class No190_颠倒二进制位 {
    public static void main(String[] args) {
        Solution190 solution190 = new Solution190();
        int reverseBits = solution190.reverseBits(43261596);
        System.out.println();
    }
}

class Solution190 {
    public int reverseBits(int n) {
        //逐个反转
        int res = n & 1;
        res = res << 31;//1000
        for (int i = 1; i <= 31; i++) {
            int a = n >> i;
            int b = a & 1;
            int c = b << (31 - i);//0100
            res = res | c;
        }
        return res;
    }
}

    //public int reverseBits(int n) {
    //    //位运算
    //    n = (n << 16) | (n >>> 16);
    //    n = (n << 8 & 0xff00ff00) | (n >>> 8 & 0x00ff00ff);
    //    n = (n << 4 & 0xf0f0f0f0) | (n >>> 4 & 0x0f0f0f0f);
    //    n = (n << 2 & 0xcccccccc) | (n >>> 2 & 0x33333333);
    //    n = (n << 1 & 0xaaaaaaaa) | (n >>> 1 & 0x55555555);
    //    return n;
    //}


    //public int reverseBits(int n) {
    //    //按位颠倒
    //    int res = 0;
    //    for (int i = 0; i < 32; i++) {
    //        //n & 1:获取最后位的数字
    //        res = (res << 1) + (n & 1);
    //        n = n >> 1;
    //    }
    //    return res;
    //}