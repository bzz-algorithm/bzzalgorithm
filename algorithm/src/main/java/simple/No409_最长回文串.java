package simple;

import java.util.HashMap;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/1/18 22:17
 */
public class No409_最长回文串 {
    public static void main(String[] args) {
        Solution409 solution409 = new Solution409();
        int i = solution409.longestPalindrome("");
        System.out.println(i);
    }
}

class Solution409 {
    public int longestPalindrome(String s) {
        //一次遍历
        //如何给a放确定的位置
        // index - 'a' 类似这种操作?
        //由于有小写和大写,所以定义52长度的数组
        int[] save = new int[52];
        // 'a' 'A' 对应的ASCII 码
        //A-Z: 65 -90  //save[0-25] 这里存大写
        //a-z: 97-122 // save[26-51] 这里存小写

        //1的个数
        int one = 0;
        //相同字母的对数
        int count = 0;

        //charAt() 获取 char check
        for (int i = 0; i < s.length(); i++) {
            char check = s.charAt(i);
            if ((int) check < 97) {
                //大写
                //对check 进行判断大小写 如果是大写: check - 65
                if (save[check - 65] == 0) {
                    //0->1
                    one += 1;
                    //不进行count处理
                    //count += 0;
                    save[check - 65] = 1;
                } else {
                    //1->0
                    one -= 1;
                    count++;
                    save[check - 65] = 0;
                }
            } else {
                //小写
                //对check 进行判断大小写 如果是小写: check - 71
                if (save[check - 71] == 0) {
                    one += 1;
                    save[check - 71] = 1;
                } else {
                    save[check - 71] = 0;
                    count++;
                    one -= 1;
                }
            }

        }

        //判断one和count
        if (one == 0) {
            //全是偶数个相同字母
            return count * 2;
        } else {
            return count * 2 + 1;
        }

    }

}



//public int longestPalindrome(String s) {
//        //abccccdd
//        //ccdadcc = 7
//        //ccdbdcc = 7
//        if ("".equals(s.trim())) {
//            return 0;
//        }
//
//        Map<Character, Integer> map = new HashMap<>();
//        //单词统计
//        //遍历s
//        for (char c : s.toCharArray()) {
//            addMapKeys(map, c);
//        }
//
//        //false:代表全是偶数
//        boolean flag = false;
//        int res = 0;
//
//        //遍历Map
//        for (Map.Entry<Character, Integer> data : map.entrySet()) {
//            if (data.getValue() % 2 != 0) {
//                flag = true;
//                res += data.getValue() - 1;
//            } else {
//                res += data.getValue();
//            }
//        }
//
//        if (flag) {
//            //奇数
//            return res + 1;
//        } else {
//            return res;
//        }
//
//    }
//
//    public void addMapKeys(Map<Character, Integer> map, char key) {
//        if (map.get(key) == null) {
//            map.put(key, 1);
//        } else {
//            map.put(key, map.get(key) + 1);
//        }
//    }




