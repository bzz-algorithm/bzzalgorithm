package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/11/2 21:42
 */
public class No371_两整数之和 {
    public static void main(String[] args) {
        Solution371 solution371 = new Solution371();
        int sum = solution371.getSum(27, 10);
        System.out.println(sum);
    }
}

class Solution371 {
    public int getSum(int a, int b) {
        //位运算
        //获取无进位
        int wu = a ^ b;
        //获取有进位
        int you = (a & b) << 1;
        while ((wu & you) != 0) {
            a = wu;
            b = you;
            wu = a ^ b;
            you = (a & b) << 1;
        }
        return wu | you;
    }
}
