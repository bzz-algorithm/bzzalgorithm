package simple;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/8/1 22:38
 */
public class No231_2的幂 {
    public static void main(String[] args) {
        Solution231 solution231 = new Solution231();
        boolean powerOfTwo = solution231.isPowerOfTwo(1);
        System.out.println(powerOfTwo);
    }
}

class Solution231 {
    public boolean isPowerOfTwo(int n) {
        //与运算,O(1)时间复杂度
        if (n <= 0) {
            return false;
        }
        return (n & (-n)) == n;
    }
}


    //public boolean isPowerOfTwo(int n) {
    //    if (n == 0) {
    //        return false;
    //    }
    //    //位运算
    //    while (n != 1) {
    //        //获取位最后一位数字
    //        int shu = n & 1;
    //        if (shu != 0) {
    //            return false;
    //        }
    //        n = n >> 1;
    //    }
    //    return true;
    //}


    //public boolean isPowerOfTwo(int n) {
    //    if (n == 0) {
    //        return false;
    //    }
    //    //短除法
    //    while (n != 1) {
    //        //判断余数
    //        int yu = n % 2;
    //        if (yu != 0) {
    //            return false;
    //        }
    //        n = n / 2;
    //    }
    //    return true;
    //}