```java
//将一个给定字符串 s 根据给定的行数 numRows ，以从上往下、从左到右进行 Z 字形排列。
//之后，你的输出需要从左往右逐行读取，产生出一个新的字符串

//如5行,字符串abcdefghijklmnopq
//
//a     i     q 
//b   h j   p 
//c  g  k  o  
//d f   l n
//e     m
//输出这玩意:
//aiqbhjpcgkodflnem
```