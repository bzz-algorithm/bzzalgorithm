```java
//以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [start, end] 。请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。

//栗:
//[[5,8],[4,9],[1,2]]
//结果:[[4,9],[1,2]]

//1 <= intervals.length <= 10000
//intervals[i].length == 2
//0 <= starti <= endi <= 10000


```