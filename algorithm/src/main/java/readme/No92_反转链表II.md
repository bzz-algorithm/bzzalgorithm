```java
// 给定一个链表和其节点编号left,right
//请你写一算法,将[left,right]位置的链表反转
//返回反转之后的链表
//栗:
// 1,2,7,9,5  left=2,right=4

//返回 1, 9,7,2, 5

//1<=left<=right
```