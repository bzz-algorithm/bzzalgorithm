```java
//给定一个整数n
//请你生成所有1-n组成的二叉搜索树

//二叉搜索树定义
// 左子节点值<根节点
// 右子节点值>根节点

//栗: n=3
//1 | 1   |  2 
// 3|  2  | 1 3
//2 |   3 |

//  3 |   3
// 2  | 1
//1   |   2

// 5种
//1<=n<=8
```