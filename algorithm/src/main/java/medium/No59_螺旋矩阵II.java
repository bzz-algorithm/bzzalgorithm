package medium;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/12/23 19:13
 */
public class No59_螺旋矩阵II {
    public static void main(String[] args) {
        Solution59 solution59 = new Solution59();
        int n = 5;
        int[][] ints = solution59.generateMatrix(n);
        System.out.println(ints);
    }
}

class Solution59 {
    public int[][] generateMatrix(int n) {
        //迭代
        //方向表
        int[][] de = new int[][]{
                {0, 1},
                {1, 0},
                {0, -1},
                {-1, 0}
        };
        
        //矩阵元素值
        int dataNum = 1;
        //分区计算值,用于计算方向表索引,从而获取下一次x,y的迭代值
        int qu = 0;
        int jie = n;
        int x = 0;
        int y = 0;

        int[][] res = new int[n][n];
        if (n == 1) {
            res[0][0] = 1;
            return res;
        }
        
        //开始处理
        while (dataNum <= n * n) {

            if (qu == 4 * (jie - 1)) {
                //说明在下个环
                qu = 0;
                x++;
                y++;
                //缩阶
                jie -= 2; //如果n=1,jie=-1
                if (jie == 1) {
                    res[x][y] = dataNum;
                    break;
                }
            } else {
                res[x][y] = dataNum;
                //获取方向表索引
                int fangxiangIndex = qu / (jie - 1);
    
                x = x + de[fangxiangIndex][0];
                y = y + de[fangxiangIndex][1];
                dataNum++;
                qu++;
            }
        }
        return res;
    }
}



    //public int[][] generateMatrix(int n) {
    //    //方向表整合
    //    //每次都是一个同心环,因此使用递归
    //
    //    int[][] res = new int[n][n];
    //    //方向表
    //    int[][] de = new int[][]{
    //            {0, 1},
    //            {1, 0},
    //            {0, -1},
    //            {-1, 0}
    //    };
    //    generateMatrix(n, res, 0, 0, de, 0);
    //
    //    return res;
    //}
    //
    ////n:阶级
    ////res:结果矩阵
    ////x,y 环的左上角起点
    ////de:方向表:{{a,b},{c,d}...}
    //public void generateMatrix(int n, int[][] res, int x, int y, int[][] de,int dataNum) {
    //    
    //    //递归终止条件
    //    if (n <= 0) {
    //        return;
    //    }
    //
    //    if (n == 1) {
    //        res[x][y] = dataNum + 1;
    //        return;
    //    }
    //    
    //    //拿到环,开始写数字和代码
    //    //对于每一个环,获取数字---> 分区计算值用于计算方向分区
    //    for (int i = 0; i < (n - 1) * 4; i++) {
    //        //获取方向表索引判断哪个方向
    //        int fangxiangIndex = i / (n - 1);
    //
    //        res[x][y] = dataNum + 1;
    //        
    //        x = x + de[fangxiangIndex][0];
    //        y = y + de[fangxiangIndex][1];
    //        dataNum++;
    //    }
    //    generateMatrix(n - 2, res, x + 1, y + 1, de, dataNum);
    //}

