package medium;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2022/3/12 9:53
 */
public class No74_搜索二维矩阵 {
    public static void main(String[] args) {
        Solution74 solution74 = new Solution74();
        int[][] matrix = new int[][]{
                {5, 7, 8, 9},
                {13, 14, 17, 23},
                {29, 46, 67, 88}

        };

        solution74.searchMatrix(matrix, 13);
    }
}


class Solution74 {
    public boolean searchMatrix(int[][] matrix, int target) {
        // 二分查找
        int a = 0;
        int b = matrix.length - 1;
        int mid = 0;
        while (a < b) {
            mid = a + (b - a) / 2;
            //开始对比,移动a,b
            if (target > matrix[mid][matrix[0].length - 1]) {
                a = mid + 1;
            } else if (target < matrix[mid][matrix[0].length - 1]) {
                b = mid - 1;
            } else {
                return true;
            } 
        }
        
        //a>=b,注意a,b绝对值不会超过2
        //投机取巧,不要分析,无脑运行
        for (int i = mid - 2; i <= mid + 2; i++) {
            //这里其实可以再二分查找
            for (int j = 0; j < matrix[0].length; j++) {
                if (i >= 0 && j >= 0 && i <= matrix.length - 1 && j <= matrix[0].length - 1
                        && matrix[i][j] == target) {
                    return true;
                }
            }
        }
        return false;
    }
}



    //public boolean searchMatrix(int[][] matrix, int target) {
    //    //值所在行
    //    int 值所在行 = 0;
    //    while (值所在行 < matrix.length) {
    //        if (matrix[值所在行][matrix[0].length - 1] >= target) {
    //            //就在值所在行
    //            for (int j = 0; j < matrix[0].length; j++) {
    //                if (matrix[值所在行][j] == target) {
    //                    return true;
    //                }
    //            }
    //        }
    //        
    //        值所在行++;
    //    }
    //    return false;
    //}



    //public boolean searchMatrix(int[][] matrix, int target) {
    //    // 暴力法
    //    for (int i = 0; i < matrix.length; i++) {
    //        for (int j = 0; j < matrix[0].length; j++) {
    //            if (matrix[i][j] == target) {
    //                return true;
    //            }
    //        }
    //    }
    //    return false;
    //}