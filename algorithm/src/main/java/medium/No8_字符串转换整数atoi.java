package medium;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/6/28 21:29
 */
public class No8_字符串转换整数atoi {
    public static void main(String[] args) {
        Solution8 solution8 = new Solution8();
        int i = solution8.myAtoi("     +424");
        System.out.println(i);
    }
}

class Solution8 {
    public int myAtoi(String s) {
        //前导空格处理
        s = s.trim();
        //正则匹配
        String regex = "(^[+-]?\\d+)";
        Pattern p = Pattern.compile(regex);
        //匹配
        Matcher matcher = p.matcher(s);
        //查询匹配
        //0:为了后面匹配不到,拿默认值
        String data = "0";
        if (matcher.find()) {
            data = matcher.group(0);
        }
        //如果data带符号,如果它带+,不理他,如果它带- {考虑越界问题}
        try {
            return Integer.valueOf(data);
        } catch (Exception e) {
            return data.charAt(0) == '-' ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        }
    }
}



    //public int myAtoi(String s) {
    //    //有限状态自动机????
    //    //状态转移路径
    //    //根据分析,制作状态表格
    //    
    //    // 定义 '' 列 0 +- 排第1位 0-9 排第2 .a-Z 排第3
    //    //     ''     +-     0-9     .a-Z
    //    //0:    0     1      2         3
    //    //1:    3     3      2         3
    //    //2:    3     3      2         3
    //    //3:    3     3      3         3
    //    
    //    //0:初始状态  1:正负状态  2:整数状态  3:结束状态
    //    Map<String, Map<String, String>> stateMap = initMap();
    //    //标记状态
    //    String state = "初始状态";
    //    int res = 0;
    //    //正负标记
    //    int sign = 1;
    //    //开始遍历
    //    for (int i = 0; i < s.length(); i++) {
    //        //每一个输入
    //        char check = s.charAt(i);
    //        //开始状态转移
    //        if (check == ' ') {
    //            state = stateMap.get(state).get("空格");
    //        } else if (check == '+' || check == '-') {
    //            if (check == '-') {
    //                sign = -1;
    //            }
    //            state = stateMap.get(state).get("+-");
    //        } else if (check >= '0' && check <= '9') {
    //            state = stateMap.get(state).get("数字");
    //        } else if (check == '.' || (check >= 'a' && check <= 'z') ||
    //                (check >= 'A' && check <= 'Z')) {
    //            state = stateMap.get(state).get(".或字母");
    //        }
    //        
    //        //如果最后state处于结束状态
    //        if (state.equals("结束状态")) {
    //            break;
    //        }
    //        
    //        //开始获取整数:整数状态
    //        if (state.equals("整数状态")) {
    //            //注意:判断越界问题
    //            
    //            //res变为负:正负处理
    //            res = Math.abs(res) * sign;
    //            int ge = check - '0';
    //            if (sign == 1) {
    //                //越界判断
    //                if (res > Integer.MAX_VALUE / 10 ||
    //                        (res == Integer.MAX_VALUE/10 && ge > 7)) {
    //                    return Integer.MAX_VALUE;
    //                }
    //                //正常获取
    //                res = res * 10 + ge;
    //            } else {
    //                if (res < Integer.MIN_VALUE/10 ||
    //                        (res == Integer.MIN_VALUE/10 && ge > 8)) {
    //                    return Integer.MIN_VALUE;
    //                }
    //                res = res * 10 - ge;
    //            } 
    //        }
    //        
    //    }
    //    return res;
    //}
    //
    //public Map<String, Map<String, String>> initMap() {
    //    //初始状态
    //    Map<String, String> map0 = new HashMap<>();
    //    map0.put("空格", "初始状态");
    //    map0.put("+-", "正负状态");
    //    map0.put("数字", "整数状态");
    //    map0.put(".或字母", "结束状态");
    //    
    //    //正负状态
    //    Map<String, String> map1 = new HashMap<>();
    //    map1.put("空格", "结束状态");
    //    map1.put("+-", "结束状态");
    //    map1.put("数字", "整数状态");
    //    map1.put(".或字母", "结束状态");
    //    
    //    //整数状态
    //    Map<String, String> map2 = new HashMap<>();
    //    map2.put("空格", "结束状态");
    //    map2.put("+-", "结束状态");
    //    map2.put("数字", "整数状态");
    //    map2.put(".或字母", "结束状态");
    //    
    //    //结束状态
    //    Map<String, String> map3 = new HashMap<>();
    //    map3.put("空格", "结束状态");
    //    map3.put("+-", "结束状态");
    //    map3.put("数字", "结束状态");
    //    map3.put(".或字母", "结束状态");
    //
    //    Map<String, Map<String, String>> res = new HashMap<>();
    //    res.put("初始状态", map0);
    //    res.put("正负状态", map1);
    //    res.put("整数状态", map2);
    //    res.put("结束状态", map3);
    //    return res;
    //}





