package medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2022/2/26 9:21
 */
public class No71_简化路径 {
    public static void main(String[] args) {
        Solution71 solution71 = new Solution71();
        //String path = "/a/../././x/.///b/c/../../d/";
        String path = "/..asldf";
        String s = solution71.simplifyPath(path);
        System.out.println(s);
    }
}

class Solution71 {
    public String simplifyPath(String path) {
        // 有限状态自动机
        // 8
        //初始化状态变化表
        Map<String, Map<Character, String>> stateMap = initStateMap();
        String currState = "当前状态";
        String preState = currState;
        String res = "/";
        
        //注意,由于都是在/位置结算,因此如果给你的用例没有/则无法结算
        //因此,人为添加/,强行结算即可
        path = path + "/";
        
        //开始循环获取状态
        for (int i = 1; i < path.length(); i++) {
            //当前遍历的字符
            char check = path.charAt(i);
            //保存变更之前的状态用于结束状态结算
            preState = currState;
            //状态变更
            if ('/' != check && '.' != check) {
                //字母(非/.)
                currState = stateMap.get(currState).get('o');
            } else {
                currState = stateMap.get(currState).get(check);
            } 

            //无脑加值即可
            res += check;
            if ("结束状态".equals(currState)) {
                //开始结算,需要判断从哪个状态到的结束状态,用到了preState
                if ("当前状态".equals(preState)) {
                    //  /xx/xx////// -> /xx/xx/
                    //注意,这里要去掉加的/
                    res = res.substring(0, res.length() - 1);
                } else if ("预备状态".equals(preState)) {
                    //  /xx/./ -> /xx/
                    //  /./  -> /
                    res = res.substring(0, res.length() - 2);
                } else if ("上一级状态".equals(preState)) {
                    //  /../ -> /
                    //  /xx/../ -> /
                    if ("/../".equals(res)) {
                        res = "/";
                    } else {
                        // /xx/xx/xx/../ -> /xx/xx/
                        ///xx/xx/xx -> /xx/xx/
                        res = res.substring(0, res.length() - 4);
                        int _index = res.lastIndexOf("/");
                        res = res.substring(0, _index + 1);
                    }

                } else if ("下一级状态".equals(preState)) {
                    //不处理
                }
                currState = "当前状态";
            }
        }
        
        //这里注意,由于在结束状态结算,因此 有可能 /   
        // /xx/
        if (res.equals("/")) {
            return "/";
        } else {
            return res.substring(0, res.length() - 1);
        } 

    }

    public Map<String, Map<Character, String>> initStateMap() {
        //获取状态变化:map.get(state).get(check)
        //当前状态
        Map<Character, String> map1 = new HashMap<>();
        map1.put('.', "预备状态");
        map1.put('/', "结束状态");
        map1.put('o', "下一级状态");
        
        //预备状态
        Map<Character, String> map2 = new HashMap<>();
        map2.put('.', "上一级状态");
        map2.put('o', "下一级状态");
        map2.put('/', "结束状态");
        
        //上一级状态
        Map<Character, String> map3 = new HashMap<>();
        map3.put('.', "下一级状态");
        map3.put('o', "下一级状态");
        map3.put('/', "结束状态");
        
        //下一级状态
        Map<Character, String> map4 = new HashMap<>();
        map4.put('.', "下一级状态");
        map4.put('o', "下一级状态");
        map4.put('/', "结束状态");
        
        Map<String, Map<Character, String>> res = new HashMap<>();
        res.put("当前状态", map1);
        res.put("预备状态", map2);
        res.put("上一级状态", map3);
        res.put("下一级状态", map4);
        return res;
    }
}



    //public String simplifyPath(String path) {
    //    if (path.length() <= 1) {
    //        return "/";
    //    }
    //    //切勿忘记,因为为了方便处理
    //    path = path + "/";
    //    // 空间优化
    //    //代表:之前的索引,用于获取muluName
    //    int preIndex = 0;
    //    List<String> muludeepList = new ArrayList<>();
    //    for (int i = 1; i < path.length(); i++) {
    //        String check = path.charAt(i) + "";
    //        //如果check /,说明找到了下一个/
    //        if ("/".equals(check)) {
    //            //找到之后,获取muluName
    //            String muluName = path.substring(preIndex + 1, i);
    //            //根据muluName操作list
    //            dealList(muludeepList, muluName);
    //            //下一轮预备
    //            preIndex = i;
    //        }
    //    }
    //    
    //    //list结束
    //    StringBuffer stringBuffer = new StringBuffer();
    //    if (muludeepList.size() == 0) {
    //        return "/";
    //    } else {
    //        for (String mulu : muludeepList) {
    //            stringBuffer.append("/").append(mulu);
    //        }
    //        return stringBuffer.toString();
    //    } 
    //}
    //
    ////根据两个/ 之间的值,操作list
    //public void dealList(List<String> muludeepList, String muluName) {
    //    if (".".equals(muluName) || "".equals(muluName)) {
    //        return;
    //    } else {
    //        if ("..".equals(muluName)) {
    //            if (muludeepList.size() > 0) {
    //                muludeepList.remove(muludeepList.size() - 1);
    //            }
    //        } else {
    //            //a,b,c
    //            muludeepList.add(muluName);
    //        } 
    //    } 
    //}



    //public String simplifyPath(String path) {
    //    //  /a/../././x/.///b/c/../../d/... -> /x/d/...
    //    // 逐步演示
    //    //path /切割获取每一步操作
    //    String[] splits = path.split("/");
    //    //定义list存放当前目录深度
    //    //muludeepList:  -> /    a,b,c -> /a/b/c
    //    List<String> muludeepList = new ArrayList<>();
    //    //遍历splits
    //    for (String muluName : splits) {
    //        if (".".equals(muluName) || "".equals(muluName)) {
    //            continue;
    //        } else {
    //            //先考虑..情况
    //            if ("..".equals(muluName)) {
    //                //list移除
    //                if (muludeepList.size() > 0) {
    //                    muludeepList.remove(muludeepList.size() - 1);
    //                }
    //            } else {
    //                //a,b,c
    //                muludeepList.add(muluName);
    //            } 
    //            //a,b,c这种情况,可能?? ..
    //        } 
    //    }
    //    
    //    //list: -> / 
    //    StringBuffer stringBuffer = new StringBuffer();
    //    if (muludeepList.size() == 0) {
    //        return "/";
    //    } else {
    //        //拼接即可
    //        for (String mulu : muludeepList) {
    //            stringBuffer.append("/").append(mulu);
    //        }
    //        return stringBuffer.toString();
    //    } 
    //}








