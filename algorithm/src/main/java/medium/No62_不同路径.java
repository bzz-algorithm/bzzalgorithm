package medium;

import java.util.Arrays;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2022/1/9 18:53
 */
public class No62_不同路径 {
    public static void main(String[] args) {
        Solution62 solution62 = new Solution62();
        int i = solution62.uniquePaths(3, 7);
        System.out.println(i);
    }
}

class Solution62 {
    public int uniquePaths(int m, int n) {
        //一维数组,利用了动态规划的规律
        //定义一维数组
        int[] res = new int[n];
        res[0] = 1;
        //开始迭代
        for(int i = 0;i<m;i++) {//迭代m次
            for (int j = 1; j < n; j++) {
                res[j] = res[j - 1] + res[j];
            }
        }
        return res[n - 1];
    }
}



    //public int uniquePaths(int m, int n) {
    //    //动态规划
    //    //定义:dp[x][y]代表从(0,0)开始到(x,y)的路径条
    //    int[][] dp = new int[m][n];
    //    dp[0][0] = 1;
    //    
    //    //开始遍历获取每个格子的路径条
    //    for (int x = 0; x < m; x++) {
    //        for (int y = 0; y < n; y++) {
    //            //开始处理
    //            //dp[x][y] = dp[x][y - 1] + dp[x - 1][y];
    //            //以上方程式注意边界溢出问题
    //            //只有y-1,x-1>=0才参与计算
    //            if (y - 1 >= 0) {
    //                dp[x][y] = dp[x][y - 1];
    //            }
    //            if (x - 1 >= 0) {
    //                dp[x][y] += dp[x - 1][y];
    //            }
    //        }
    //    }
    //    return dp[m - 1][n - 1];
    //}






