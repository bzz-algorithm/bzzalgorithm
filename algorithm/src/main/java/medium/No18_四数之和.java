package medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/8/7 17:17
 */
public class No18_四数之和 {
    public static void main(String[] args) {
        Solution18 solution18 = new Solution18();
        int[] nums = new int[]{4, 7, 9, 2, 5, 1, -3, -3, 8};
        List<List<Integer>> lists = solution18.fourSum(nums, 12);
        System.out.println();
    }
}


class Solution18 {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums.length < 4) {
            return res;
        }
        //方法可参考:No15.三数之和
        //元组不重复,里面的元素可以重复
        Arrays.sort(nums);
        int length = nums.length;
        //大指针,bigA,bigB
        for (int bigA = 0; bigA < nums.length; bigA++) {
            //由于有4个元素,这里也要考虑
            if (bigA > 0 && nums[bigA] == nums[bigA - 1]) {
                continue;
            }
            for (int bigB = bigA + 1; bigB < length - 2; bigB++) {
                int red = bigB + 1;
                int green = nums.length - 1;
                //bigB优化
                if (bigB > bigA + 1 && nums[bigB] == nums[bigB - 1]) {
                    continue;
                }
                
                //可以处理
                while (red < green) {
                    //对red,green与bigA,bigB相同的处理方式
                    if (red > bigB + 1 && nums[red] == nums[red - 1]) {
                        red++;
                        continue;
                    }
                    if (green < nums.length - 1 && nums[green] == nums[green + 1]) {
                        green--;
                        continue;
                    }
                    
                    //经过上述处理,a,b,c,d值的指针位置互不相同
                    int a = nums[bigA];
                    int b = nums[bigB];
                    int c = nums[red];
                    int d = nums[green];
                    int check = target - (a + b);
                    
                    //开始处理
                    if (c + d > check) {
                        green--;
                    } else if (c + d < check) {
                        red++;
                    } else {
                        //可以加
                        List<Integer> list = new ArrayList<>();
                        list.add(a);
                        list.add(b);
                        list.add(c);
                        list.add(d);
                        res.add(list);
                        red++;
                        green--;
                    } 
                    
                }
            }
        }
        return res;
    }
}









