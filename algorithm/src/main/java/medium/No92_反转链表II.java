package medium;

import data.ListNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2022/6/18 14:43
 */
public class No92_反转链表II {
    public static void main(String[] args) {
        Solution92 solution92 = new Solution92();
        ListNode head = new ListNode(1);
        head.add(2);
        head.add(7);
        head.add(9);
        head.add(5);

        ListNode listNode = solution92.reverseBetween(head, 1, 2);
        System.out.println(listNode);
    }
}

class Solution92 {
    public ListNode reverseBetween(ListNode head, int left, int right) {
        ListNode headCp = new ListNode(-1);
        headCp.next = head;
        ListNode teamLeader = headCp;
        int count = 0;
        
        //定义插入位置
        ListNode g = headCp;
        while (headCp != null) {
            headCp = headCp.next;
            count++;
            if (count == left - 1) {
                g = headCp;
            }
            //获取循环条件[)
            while (count >= left && count < right) {
                //开始处理
                ListNode A = headCp;
                ListNode B = A.next;
                A.next = B.next;
                B.next = g.next;
                g.next = B;
                //以上操作会顺带将headCp 往下一个
                count++;
            }
        }
        return teamLeader.next;
    }
}



    //public ListNode reverseBetween(ListNode head, int left, int right) {
    //    ListNode headCp = new ListNode(-1);
    //    headCp.next = head;
    //    int count = 0;
    //    //开始遍历headCp处理
    //    ListNode g1 = head;
    //    ListNode g2 = head;
    //    ListNode preG1 = headCp;
    //    ListNode nextG2 = head.next;
    //    //由于下面headCp遍历获取不到了,故使用teamLeader当做新头
    //    ListNode teamLeader = headCp;
    //    
    //    //如果right在链表最后,则按照模板,无法获取nextG2
    //    while (headCp != null) {
    //        headCp = headCp.next;
    //        count++;
    //        //开始获取left,right位置
    //        if (count == left) {
    //            //headCp就是g1
    //            g1 = headCp;
    //        }
    //        if (count == right) {
    //            g2 = headCp;
    //        }
    //        if (count == left - 1) {
    //            preG1 = headCp;
    //        }
    //        if (count == right + 1) {
    //            nextG2 = headCp;
    //        }
    //    }
    //    
    //    //将链表反转
    //    // g1 -> g2
    //    g2.next = null;
    //    reverseList(g1);
    //    preG1.next = g2;
    //    g1.next = nextG2;
    //    return teamLeader.next;
    //}
    //
    //
    ////将链表反转
    //public ListNode reverseList(ListNode listNode) {
    //    ListNode red = null;
    //    ListNode green = listNode;
    //    while (green != null) {
    //        //开始逐个反转
    //        //首先定义后续的黄色指针
    //        ListNode yellow = green.next;
    //        green.next = red;
    //        red = green;
    //        green = yellow;
    //    }
    //    return red;
    //}



    //public ListNode reverseBetween(ListNode head, int left, int right) {
    //    List<Integer> data = new ArrayList<>();
    //    
    //    while (head != null) {
    //        data.add(head.val);
    //        head = head.next;
    //    }
    //    
    //    // data:A,B,C,D,E
    //    while (left <= right) {
    //        //将list left和right位置交换
    //        //left 位置值
    //        int a = data.get(left - 1);
    //        //right 位置值
    //        int b = data.get(right - 1);
    //        data.set(left - 1, b);
    //        data.set(right - 1, a);
    //        //指针移动
    //        left++;
    //        right--;
    //    }
    //    
    //    // data:A,D,C,B,E
    //    //data转链表
    //
    //    ListNode res = new ListNode(-1);
    //
    //    for (int i = 0; i < data.size(); i++) {
    //        list2LianBIAO(res, data.get(i));
    //    }
    //    return res.next;
    //}
    //
    ////将data加入到链表-> listNode最后加一个data
    //public void list2LianBIAO(ListNode listNode, int data) {
    //    ListNode headCp = new ListNode(-1);
    //    headCp.next = listNode;
    //    while (headCp != null && headCp.next != null) {
    //        headCp = headCp.next;
    //    }
    //    headCp.next = new ListNode(data);
    //}
