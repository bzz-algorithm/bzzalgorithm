package medium;

import data.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2022/7/11 18:47
 */
public class No95_不同的二叉搜索树II {
    public static void main(String[] args) {
        Solution95 solution95 = new Solution95();
        List<TreeNode> list = solution95.generateTrees(7);
        System.out.println(list);
    }
}

class Solution95 {
    public List<TreeNode> generateTrees(int n) {
        // 动态规划
    }
}



    //public List<TreeNode> generateTrees(int n) {
    //    return generateTrees(1, n);
    //}
    //
    //public List<TreeNode> generateTrees(int start, int end) {
    //    //注意:每一层递归res都是独立的,不会共用
    //    List<TreeNode> res = new ArrayList<>();
    //    if (start > end) {
    //        //不能直接这样写,因为for会拿不到
    //        res.add(null);
    //        return res;
    //    }
    //    for (int i = start; i <= end; i++) {
    //        //当前i=76
    //        //疯狂递归获取76的左子树,和76的右子树
    //        
    //        //获取左子树
    //        List<TreeNode> leftTrees = generateTrees(start, i - 1);
    //        //获取右子树
    //        List<TreeNode> rightTrees = generateTrees(i + 1, end);
    //
    //        //遍历获取组合树
    //        for (int j = 0; j < leftTrees.size(); j++) {
    //            for (int k = 0; k < rightTrees.size(); k++) {
    //                //每一个j组合k
    //                TreeNode root = new TreeNode(i);
    //                root.left = leftTrees.get(j);
    //                root.right = rightTrees.get(k);
    //                res.add(root);
    //            }
    //        }
    //    }
    //    return res;
    //}






