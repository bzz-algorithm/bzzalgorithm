package medium;


import java.math.BigDecimal;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/10/4 15:45
 */
public class No43_字符串相乘 {
    public static void main(String[] args) {
        Solution43 solution43 = new Solution43();
        String multiply = solution43.multiply("324", "174");
        System.out.println(multiply);
    }
}

class Solution43 {
    public String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        String res = "0";
        //769*27 = 63 + 420 + 4900 + 180 + 1200 + 14000 = 20763
        //明细拆分
        for (int j = num2.length() - 1; j >= 0; j--) {
            int num2Ge = num2.charAt(j) - '0';
            for (int i = num1.length() - 1; i >= 0; i--) {
                int num1Ge = num1.charAt(i) - '0';
                //待加0的元素:num1Ge*num2Ge
                String base = num1Ge * num2Ge + "";
                //获取补0个数,找规律
                int lingGe = num1.length() + num2.length() - (i + 1) - (j + 1);
                for (int k = 1; k <= lingGe; k++) {
                    base += "0";
                }
                //结果加
                res = oneAdd(res, base);
            }
        }
        return res;
    }
    
    
    //加法
    public String oneAdd(String num1, String num2) {
        if (num1.equals("0")) {
            return num2;
        }
        if (num2.equals("0")) {
            return num1;
        }
        //a:长度较长,b:长度较短 如果长度相等,则a=num1
        String a = num1.length() >= num2.length() ? num1 : num2;
        String b = num1.length() < num2.length() ? num1 : num2;
        int jin = 0;
        StringBuffer res = new StringBuffer();
        //遍历短
        for (int i = 0; i < b.length(); i++) {
            int aGe = a.charAt(a.length() - 1 - i) - '0';
            int bGe = b.charAt(b.length() - 1 - i) - '0';
            int ge = (aGe + bGe + jin) % 10;
            jin = (aGe + bGe + jin) / 10;
            res.append(ge);
        }

        //长度短的遍历结束,长的需要补
        for (int i = a.length() - b.length() - 1; i >= 0; i--) {
            int aGe = a.charAt(i) - '0';
            int ge = (aGe + jin) % 10;
            jin = (aGe + jin) / 10;
            res.append(ge);
        }

        //注意最高位处理
        if (jin != 0) {
            res.append(jin);
        }

        //反转
        return res.reverse().toString();
    }
}



    //public String multiply(String num1, String num2) {
    //    if (num1.equals("0") || num2.equals("0")) {
    //        return "0";
    //    }
    //    //"4564" "4860" = "????结果值"
    //    // "7""8" "56"
    //    //return new BigDecimal(num1).multiply(new BigDecimal(num2))
    //    //        .toString();
    //    
    //    //强行硬写
    //    //分析:由于相乘结果的个位数会有进位,因此分析出来
    //    //ge = (num1Ge * num2Ge + jin ) % 10
    //    //jin = (num1Ge * num2Ge + jin ) / 10
    //    //开始组合(倒序遍历)
    //    String res = "0";
    //    for (int i = num2.length() - 1; i >= 0; i--) {
    //        //第一次,num1不会补0
    //        if (i != num2.length() - 1) {
    //            num1 += "0";
    //        }
    //        String one = num2.charAt(i) + "";
    //        //第一部分:多位数乘1位数
    //        String oneRes = oneCheng(num1, one);
    //        //第二部分:加
    //        res = oneAdd(oneRes, res);
    //    }
    //    return res;
    //}
    //
    ////加法
    //public String oneAdd(String num1, String num2) {
    //    if (num1.equals("0")) {
    //        return num2;
    //    }
    //    if (num2.equals("0")) {
    //        return num1;
    //    }
    //    //a:长度较长,b:长度较短 如果长度相等,则a=num1
    //    String a = num1.length() >= num2.length() ? num1 : num2;
    //    String b = num1.length() < num2.length() ? num1 : num2;
    //    int jin = 0;
    //    StringBuffer res = new StringBuffer();
    //    //遍历短
    //    for (int i = 0; i < b.length(); i++) {
    //        int aGe = a.charAt(a.length() - 1 - i) - '0';
    //        int bGe = b.charAt(b.length() - 1 - i) - '0';
    //        int ge = (aGe + bGe + jin) % 10;
    //        jin = (aGe + bGe + jin) / 10;
    //        res.append(ge);
    //    }
    //    
    //    //长度短的遍历结束,长的需要补
    //    for (int i = a.length() - b.length() - 1; i >= 0; i--) {
    //        int aGe = a.charAt(i) - '0';
    //        int ge = (aGe + jin) % 10;
    //        jin = (aGe + jin) / 10;
    //        res.append(ge);
    //    }
    //    
    //    //注意最高位处理
    //    if (jin != 0) {
    //        res.append(jin);
    //    }
    //    
    //    //反转
    //    return res.reverse().toString();
    //}
    //
    ////num1:num1值, one:从 num2 遍历过来的一位数
    //public String oneCheng(String num1, String one) {
    //    if (num1.equals("0") || one.equals("0")) {
    //        return "0";
    //    }
    //    //num1:倒序遍历
    //    //进位
    //    int jin = 0;
    //    //乘数拿来
    //    int cheng = one.charAt(0) - '0';
    //    //结果值
    //    StringBuffer res = new StringBuffer();
    //    for (int i = num1.length() - 1; i >= 0; i--) {
    //        int num1Ge = num1.charAt(i) - '0';
    //        //乘积之后的个位数
    //        int ge = (num1Ge * cheng + jin) % 10;
    //        jin = (num1Ge * cheng + jin) / 10;
    //        res.append(ge);
    //    }
    //    
    //    //注意最高位处理
    //    if (jin != 0) {
    //        res.append(jin);
    //    }
    //
    //    return res.reverse().toString();
    //}






