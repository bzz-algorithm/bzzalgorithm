package medium;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/11/30 20:24
 */
public class No50_PowXn {
    public static void main(String[] args) {
        Solution50 solution50 = new Solution50();
        double v = solution50.myPow(2,-2);
        System.out.println(v);
    }
}

class Solution50 {
    public double myPow(double x, int n) {
        
        //放一个判断正负的值
        boolean sign = n < 0 ? false : true;
        
        //由于java有int溢出机制,而题目如果给min,则int无解,因此使用long
        long bug = n;
        //二进制拆解
        //负数二进制转正
        //-2147483648
        if (n < 0) {
            bug = ~(bug - 1);
        }
        
        //开始二进制计算
        double res = 1.0;
        double di = x;
        while (bug != 0) {
            //拿到当前的权
            long quan = bug & 1;
            
            if (quan == 0) {
                res *= 1;
            } else {
                res *= di;
            } 
            bug = bug >> 1;
            //在遍历的时候,底数不断疯狂平方
            di = di * di;
        }
        return sign ? res : 1 / res;
    }
    
}



    //public double myPow(double x, int n) {
    //    //0.00001 2147483647
    //    //int base = n < 0 ? -n : n; //写的啥玩意???
    //    //double res = 1.0;
    //    //for (int i = 1; i <= base; i++) {
    //    //    res *= x;
    //    //}
    //    //return n < 0 ? 1 / res : res;
    //    
    //    //二分
    //    
    //    //递归走起!!
    //    //myPow(double x, int n):就是x^n
    //    
    //    //递归终止条件
    //    if (n == 1) {
    //        return x;
    //    }
    //    if (n == -1) {
    //        return 1 / x;
    //    }
    //    if (n == 0) {
    //        return 1.0;
    //    }
    //    
    //    if (n % 2 == 0) {
    //        //偶数
    //        double d = myPow(x, n / 2);//x^21
    //        return d * d;
    //    } else {
    //        //奇数
    //        double d = myPow(x, (n - 1) / 2);// x^-11
    //        return d * d * x;
    //    } 
    //    
    //}







