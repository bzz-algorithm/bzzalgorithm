package medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/7/15 21:03
 */
public class No12_整数转罗马数字 {
    public static void main(String[] args) {
        Solution12 solution12 = new Solution12();
        String s = solution12.intToRoman(2765);
        System.out.println(s);
    }
}

class Solution12 {
    public String intToRoman(int num) {
        //指针法
        //获取长度(以2765举例)
        String data = num + "";
        int length = data.length(); //4
        //获取初始中间值
        int red = 2 * length - 1; //7
        //罗马数组
        String[] lMStrings = initLM();
        StringBuffer res = new StringBuffer();
        for (char check : data.toCharArray()) {
            String lmString = getLM(lMStrings, check, red);
            res.append(lmString);
            //中间值替换
            red = red - 2;
        }
        return res.toString();
    }

    //根据数字判断如何组合字母
    public String getLM(String[] lMStrings, char check, int red) {
        int base = red - 1;
        String res = "";
        //根据数字判断组合
        if (check <= '3') {
            //获取base
            for (int i = 1; i <= check - '0'; i++) {
                res += lMStrings[base];
            }
        } else if (check == '4') {
            res += lMStrings[base];
            res += lMStrings[red];
        } else if (check <= '8') {
            res += lMStrings[red];
            //循环获取
            for (int i = 1; i <= check - '5'; i++) {
                res += lMStrings[base];
            }
        } else {
            res += lMStrings[base];
            res += lMStrings[red + 1];
        }
        return res;
    }

    //获取罗马数组
    public String[] initLM() {
        return new String[]{"I", "V", "X", "L", "C", "D", "M"};
    }
}



    //public String intToRoman(int num) {
    //    //对num从list判断,如果>=list,直接取list
    //    //将num-list值,到0即可
    //    List<String> luoMaList = initList();
    //    StringBuffer res = new StringBuffer();
    //    while (num != 0) {
    //        for (int i = luoMaList.size() - 1; i >= 0; i--) {
    //            //获取list值:1_I
    //            int listNum = Integer.valueOf(luoMaList.get(i).split("_")[0]);
    //            //罗马数字
    //            String luoMaNum = luoMaList.get(i).split("_")[1];
    //            while (num >= listNum) {
    //                num -= listNum;
    //                //结果加值
    //                res.append(luoMaNum);
    //            }
    //        }
    //    }
    //    return res.toString();
    //}
    //
    ////罗马List
    //public List<String> initList() {
    //    List<String> list = new ArrayList<>();
    //    list.add("1_I");
    //    list.add("4_IV");
    //    list.add("5_V");
    //    list.add("9_IX");
    //    
    //    list.add("10_X");
    //    list.add("40_XL");
    //    list.add("50_L");
    //    list.add("90_XC");
    //    
    //    list.add("100_C");
    //    list.add("400_CD");
    //    list.add("500_D");
    //    list.add("900_CM");
    //    
    //    list.add("1000_M");
    //    return list;
    //}



    //public String intToRoman(int num) {
    //    //1998->MCMXCVIII
    //    Map<Integer, String> luoMaMap = initMap();
    //    //获取784中的700,80,4...
    //    int ge = num % 10; //4
    //    int shi = (num - ge) % 100;//80
    //    int bai = (num - ge - shi) % 1000;
    //    int qian = num / 1000 * 1000;
    //    StringBuffer sb = new StringBuffer();
    //    sb.append(luoMaMap.get(qian));
    //    sb.append(luoMaMap.get(bai));
    //    sb.append(luoMaMap.get(shi));
    //    sb.append(luoMaMap.get(ge));
    //    
    //    //null值替换
    //    return sb.toString().replace("null", "");
    //}
    //
    ////定义罗马Map
    ////(整数,罗马数字)
    ////(1-9,罗马数字)
    ////(10-90,罗马数字)
    ////(100-900,罗马数字)
    ////(1000-3000,罗马数字)
    //public Map<Integer, String> initMap() {
    //    Map<Integer, String> map = new HashMap<>();
    //    //1-9
    //    map.put(1, "I");
    //    map.put(2, "II");
    //    map.put(3, "III");
    //    map.put(4, "IV");
    //    map.put(5, "V");
    //    map.put(6, "VI");
    //    map.put(7, "VII");
    //    map.put(8, "VIII");
    //    map.put(9, "IX");
    //
    //    //10-90
    //    map.put(10, "X");
    //    map.put(20, "XX");
    //    map.put(30, "XXX");
    //    map.put(40, "XL");
    //    map.put(50, "L");
    //    map.put(60, "LX");
    //    map.put(70, "LXX");
    //    map.put(80, "LXXX");
    //    map.put(90, "XC");
    //
    //    //100-900
    //    map.put(100, "C");
    //    map.put(200, "CC");
    //    map.put(300, "CCC");
    //    map.put(400, "CD");
    //    map.put(500, "D");
    //    map.put(600, "DC");
    //    map.put(700, "DCC");
    //    map.put(800, "DCCC");
    //    map.put(900, "CM");
    //
    //
    //    //1000-3000
    //    map.put(1000, "M");
    //    map.put(2000, "MM");
    //    map.put(3000, "MMM");
    //    return map;
    //}





