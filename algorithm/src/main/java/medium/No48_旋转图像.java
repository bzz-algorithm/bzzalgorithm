package medium;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/11/12 18:49
 */
public class No48_旋转图像 {
    public static void main(String[] args) {
        Solution48 solution48 = new Solution48();
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };
        
        solution48.rotate(matrix);
        for (int i = 0; i < matrix[0].length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}

class Solution48 {
    public void rotate(int[][] matrix) {
        // 迭代
        // 公式推导
        // x:如果有中心 (n-1)/2 -1
        // x:如果没有中心 (n-1)/2
        // 可以合并,这里方便大家理解,不合了!!!!!
        // y:?(n-1)/2
        
        //开始编码
        //n 阶
        int n = matrix.length;
        int yHe = (n - 1) / 2;
        int xHe = 973593;
        
        //根据中心判断
        if (n % 2 == 0) {
            //无中心
            xHe = (n - 1) / 2;
        } else {
            xHe = (n - 1) / 2 - 1;
        } 
        
        
        //开始大本尊!
        for (int x = 0; x <= xHe; x++) {
            for (int y = 0; y <= yHe; y++) {
                //跟上节一样
                // (x,y)->(y,n-1-x)
                int a = matrix[x][y];
                int b = matrix[y][n - 1 - x];
                int c = matrix[n - 1 - x][n - 1 - y];
                int d = matrix[n - 1 - y][n - 1 - (n - 1 - x)];

                int tmp = a;
                a = d;
                d = c;
                c = b;
                b = tmp;
                
                //赋值回去
                matrix[x][y] = a;
                matrix[y][n - 1 - x] = b;
                matrix[n - 1 - x][n - 1 - y] = c;
                matrix[n - 1 - y][n - 1 - (n - 1 - x)] = d;
            }
        }
        
    }
}



    //public void rotate(int[][] matrix) {
    //    //规律:(x,y)->(y,length-1-x)
    //    //规律:n*n 只需要遍历 (0,n-1-1)
    //    rotate(matrix, 0, 0, matrix.length);
    //}
    //
    ////使用递归处理
    ////(x,y)遍历的本尊,用于分身用
    ////n 最外框填实之后的大小
    //public void rotate(int[][] matrix, int x, int y, int n) {
    //
    //    int length = matrix.length;
    //    
    //    //经过分析,递归终止条件
    //    if (n <= 0) {
    //        return;
    //    }
    //    
    //    //外壳开始遍历
    //    for (int curY = y; curY <= y + n - 2; curY++) {
    //        //开始获取分身
    //        //规律:(x,y)->(y,length-1-x)
    //        int a = matrix[x][curY];
    //        int b = matrix[curY][length - 1 - x];
    //        int c = matrix[length - 1 - x][length - 1 - curY];
    //        int d = matrix[length - 1 - curY][length - 1 - (length - 1 - x)];
    //        //互相替换??
    //        int tmp = a;
    //        a = d;
    //        d = c;
    //        c = b;
    //        b = tmp;
    //
    //        //挤压结束,扔回
    //        matrix[x][curY] = a;
    //        matrix[curY][length - 1 - x] = b;
    //        matrix[length - 1 - x][length - 1 - curY] = c;
    //        matrix[length - 1 - curY][length - 1 - (length - 1 - x)] = d;       
    //        
    //        
    //    }
    //    rotate(matrix, x + 1, x + 1, n - 2);
    //}



    //public void rotate(int[][] matrix) {
    //    //找规律
    //    
    //    //搞一新矩阵
    //    int yLength = matrix[0].length;
    //    int xLength = matrix.length;
    //    int[][] matrixCopy = new int[xLength][yLength];
    //
    //    for (int x = 0; x < xLength; x++) {
    //        for (int y = 0; y < yLength; y++) {
    //            //获取(x,y)->(y,n-1-x)
    //            int duiX = y;
    //            int duiY = xLength - 1 - x;
    //            //对应的扔
    //            matrixCopy[duiX][duiY] = matrix[x][y];
    //        }
    //    }
    //    //扔回去
    //    for (int x = 0; x < xLength; x++) {
    //        for (int y = 0; y < yLength; y++) {
    //            matrix[x][y] = matrixCopy[x][y];
    //        }
    //    }
    //}
