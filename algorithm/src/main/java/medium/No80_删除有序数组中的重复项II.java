package medium;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2022/4/10 17:50
 */
public class No80_删除有序数组中的重复项II {
    public static void main(String[] args) {
        Solution80 solution80 = new Solution80();
        int[] nums = new int[]{1, 1, 2, 2, 2, 3, 4, 4};
        int i = solution80.removeDuplicates(nums);
        System.out.println(i);
    }
}

//参考No26方法,基本类似
class Solution80 {
    public int removeDuplicates(int[] nums) {
        //如果长度小于等于2,直接返回
        if (nums.length <= 2) {
            return nums.length;
        }
        
        //红绿指针定义指向索引2
        int red = 2;
        int green = 2;
        while (green < nums.length) {
            //开始
            if (nums[green] != nums[red - 2]) {
                //可以加,当前red加,将green位置值替换
                nums[red] = nums[green];
                red++;
            }
            green++;
        }
        return red;
    }
}



    //public int removeDuplicates(int[] nums) {
    //    // 鸡贼法
    //    //Map统计每个元素
    //    Map<Integer, Integer> treeMap = new TreeMap<>();
    //    //treeMap计算
    //    for (int num : nums) {
    //        addMapKeys(treeMap, num);
    //    }
    //
    //    int index = 0;
    //    int res = 0;
    //    //遍历treeMap
    //    for (Map.Entry<Integer, Integer> mapData : treeMap.entrySet()) {
    //        int num = mapData.getKey();
    //        int count = mapData.getValue();
    //        //每个num扔count次到nums数组
    //        res += count;
    //        for (int i = 1; i <= count; i++) {
    //            nums[index++] = num;
    //        }
    //    }
    //    return res;
    //}
    //
    ////统计
    //public void addMapKeys(Map<Integer, Integer> map, int key) {
    //    if (map.get(key) == null) {
    //        map.put(key, 1);
    //    } else {
    //        map.put(key, map.get(key) + 1);
    //    }
    //
    //    if (map.get(key) > 2) {
    //        map.put(key, 2);
    //    }
    //}





