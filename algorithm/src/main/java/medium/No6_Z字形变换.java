package medium;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2021/6/25 20:08
 */
public class No6_Z字形变换 {
    public static void main(String[] args) {
        Solution6 solution6 = new Solution6();
        String res = solution6.convert("abcdefghijklmnopq", 1);
        System.out.println(res);
    }
}

class Solution6 {
    public String convert(String s, int numRows) {
        if (numRows == 1 || s.length() <= numRows) {
            return s;
        }
            
        int base = 2 * (numRows - 1);
        //在处理的时候,区分孤零零非孤零零
        //孤零零情况:0,row-1
        //索引
        int x = 0;
        //处理0的情况
        StringBuffer stringBuffer = new StringBuffer();
        while (x < s.length()) {
            stringBuffer.append(s.charAt(x));
            x += base;
        }
        
        
        //处理非孤零零
        //遍历行号
        //每一组的第一个
        int a = 0;
        //每一组的第二个
        int b = 0;
        for (int h = 1; h < numRows - 1; h++) {
            //开始处理
            //系数,代表加几个base
            int xi = 0;
            //初始化索引为行号
            x = h;
            while (x < s.length()) {
                a = h + base * xi;
                x = a;
                if (x >= s.length()) {
                    break;
                }
                stringBuffer.append(s.charAt(x));
                b = base - h + base * xi;
                x = b;
                if (x >= s.length()) {
                    break;
                }
                stringBuffer.append(s.charAt(x));
                xi++;
            }
        }
        
        //处理row-1的情况
        x = numRows - 1;
        while (x < s.length()) {
            stringBuffer.append(s.charAt(x));
            x += base;
        }
        return stringBuffer.toString();
    }
}



    //public String convert(String s, int numRows) {
    //    //numRows=1情况排除
    //    //s:CC  numRows=6
    //    if (numRows == 1 || s.length() <= numRows) {
    //        return s;
    //    }
    //    //暴力法 
    //    //当行5行时:012343210123432101234321.......
    //    //当行4行时:0123210123210....
    //    //当行numRows时:01234...numRows-1,numRows-2......01234..
    //    //1代表上升,-1代表下降
    //    int flag = 1;
    //    //存放每一行的数组
    //    int index = 0;
    //    StringBuffer[] stringBuffers = new StringBuffer[numRows];
    //    //如何搞出:0123210.....
    //    //遍历
    //    for (int i = 0; i < s.length(); i++) {
    //        //当前要加行号的字母(该字母要加到哪行)
    //        char c = s.charAt(i);
    //        if(index==numRows-1){
    //            //顶端:-1
    //            flag = -1;
    //        }else if(index==0) {
    //            //上升
    //            flag = 1;
    //        }
    //        
    //        //行号获得,对应位置stringbuffer加字母
    //        //初始化
    //        if (stringBuffers[index] == null) {
    //            stringBuffers[index] = new StringBuffer();
    //        }
    //        stringBuffers[index].append(c);
    //        //012343210
    //        index += flag;
    //    }
    //    
    //    //最后拼接所有stringbuffer
    //    StringBuffer big = new StringBuffer();
    //    for (StringBuffer strs : stringBuffers) {
    //        big.append(strs.toString());
    //    }
    //    return big.toString();
    //}


