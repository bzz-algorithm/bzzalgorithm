package data;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/5/12 21:16
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int x) {
        val = x;
    }
}
