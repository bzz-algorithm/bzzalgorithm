package data;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2020/3/8 21:46
 */
public class ListNode {
    public int val;
    public ListNode next;
    public ListNode(int x){
        val = x;
    }

    //用于测试添加
    public void add(int data) {
        ListNode head = new ListNode(-1);
        head.next = this;
        ListNode headCp = head;
        while (head != null && head.next != null) {
            head = head.next;
        }
        head.next = new ListNode(data);
        head = headCp;
        head = head.next;
    }

    //遍历
    public void show() {
        ListNode head = new ListNode(-1);
        head.next = this;
        ListNode headCp = head;
        while (head != null && head.next != null) {
            head = head.next;
            System.out.println("ListNode Data = " + head.val);
        }
        head = headCp;
    }

}
